package com.timbre.spring.persistence.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.IProductReview;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.model.products.ProductReviewJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;
import com.timbre.sec.persistence.service.ICatalogService;
import com.timbre.sec.persistence.service.ICategoryService;
import com.timbre.sec.persistence.service.IDescriptionService;
import com.timbre.sec.persistence.service.IGalleryService;
import com.timbre.sec.persistence.service.IProductReviewService;
import com.timbre.sec.persistence.service.IProductService;
import org.slf4j.LoggerFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

  






 








@Component
@EnableTransactionManagement


//  *************************** Turn off the ApplicationListener ****************************//

/*public class ApplicationDataBaseLoadApi implements Serializable, ApplicationDataBaseLoad,
	ApplicationListener<ContextRefreshedEvent>,  ResourceLoaderAware {*/
	
	
	public class ApplicationDataBaseLoadApi implements Serializable, ApplicationDataBaseLoad
	 {
	
	
	@Value( "${db.load.option}" )
	private String loadOption;
	
	private ResourceLoader resourceLoader;
	
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
 
	public Resource getResource(String location){
		return (Resource) resourceLoader.getResource(location);
	}
	public ArrayList<ProductJPA> getProductList() {
		return ApplicationDataBaseLoadApi.productList;
	}
	
	public static ArrayList<ProductJPA> productList = new ArrayList<ProductJPA>();
	
	public static ArrayList<ProductJPA> testCatList = new ArrayList<ProductJPA>();
	
	private static ArrayList<ProductJPA> athleticTopsCatList = new ArrayList<ProductJPA>();

	public  ArrayList<ProductJPA> getAthleticTopsCatList() {
		return ApplicationDataBaseLoadApi.athleticTopsCatList;
	}

	public static void setAthleticTopsCatList(ArrayList<ProductJPA> athleticTopsCatList) {
		ApplicationDataBaseLoadApi.athleticTopsCatList = athleticTopsCatList;
	}

	private static ArrayList<ProductJPA> athleticBottomsCatList = new ArrayList<ProductJPA>();
	
	
	public  ArrayList<ProductJPA> getAthleticBottomsCatList() {
		return ApplicationDataBaseLoadApi.athleticBottomsCatList;
	}

	public static void setAthleticBottomsCatList(ArrayList<ProductJPA> athleticBottomsCatList) {
		ApplicationDataBaseLoadApi.athleticBottomsCatList = athleticBottomsCatList;
	}

	public ArrayList<ProductJPA> getTestCatList() {
		return ApplicationDataBaseLoadApi.testCatList;
	}

	public static void setTestCatList(ArrayList<ProductJPA> testCatList) {
		ApplicationDataBaseLoadApi.testCatList = testCatList;
	}

	

	public void setProductList(ArrayList<ProductJPA> productList) {
		ApplicationDataBaseLoadApi.productList = productList;
	}

	private static boolean setupDone = false;

	@Autowired
	private ApplicationContext context;
	
	

	// private JpaTestHelper testHelper = new JpaTestHelper();
	private static final Log log = LogFactory
			.getLog(ApplicationDataBaseLoadApi.class);

	@PersistenceContext
	private EntityManager entityManager;

	

	public ApplicationContext getContext() {
		return context;
	}

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Autowired
	IProductService productService;

	public IProductService getProductService() {
		return productService;
	}

	public void setProductService(IProductService productService) {
		this.productService = productService;
	}

	@Autowired
	ICategoryService categoryService;

	public ICategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategroyService(ICategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@Autowired
	ICatalogService catalogService;

	public ICatalogService getCatalogService() {
		return catalogService;
	}

	public void setCatalogService(ICatalogService catalogService) {
		this.catalogService = catalogService;
	}

	@Autowired
	IDescriptionService descriptionService;

	public IDescriptionService getDescriptionService() {
		return descriptionService;
	}

	public void setDescriptionService(IDescriptionService descriptionService) {
		this.descriptionService = descriptionService;
	}

	@Autowired
	IProductReviewService productReviewService;

	public IProductReviewService getProductReviewServices() {
		return productReviewService;
	}

	public void setProductReviewServices(
			IProductReviewService productReviewService) {
		this.productReviewService = productReviewService;
	}

	@Autowired
	IGalleryService galleryService;

	public IGalleryService getGalleryService() {
		return galleryService;
	}

	public void setGalleryService(IGalleryService galleryService) {
		this.galleryService = galleryService;
	}

	//XMLUtility xmlUtil = new XMLUtility();

	final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(ApplicationDataBaseLoadApi.class);

	//private EntityFactoryJPA factory = new EntityFactoryJPA();
	
	private static final long serialVersionUID = 1L;
  
    
    
    private String testValue = "testValue";

	
    
    //public static DynamicImageController dynamicImageController;
  
    public String getTestValue() {
		return testValue;
	}

	public void setTestValue(String testValue) {
		this.testValue = testValue;
	}

	
	
	public ApplicationDataBaseLoadApi() {
			//super();
	}

	
	
	public static ArrayList<String> fileList;
	public static ArrayList<String>categoryList;
	public static String categoryName;
	public static String fileName = null;
	static {
		
		fileList = new ArrayList<String>();
		fileList.add("AthleticTopsDescNov19.xml");
		fileList.add("AthleticBottomsDescNov19.xml");
		
		categoryList = new ArrayList<String>();
		categoryList.add("AthleticTopsDescNov19.xml");
		categoryList.add("AthleticBottomsDescNov19.xml");
		
		
		
		// ConfigurableApplicationContext context = new
		// ClassPathXmlApplicationContext("app-contextConfig.xml");
		// MyApplicationContextInitializer MyApplicationContextInitializer = new
		// MyApplicationContextInitializer();
		
	}
	
	/*@Transactional
	@Override
	
	  public void onApplicationEvent(ContextRefreshedEvent event) {
		
		System.out.println("Context Refreshed");
		//event.setEventFired(true);
		
		if (!setupDone) {
			
			ApplicationContext applicationContext = event.getApplicationContext();
			// could load the description files as a resource
			// This one does not load the UI
			System.out
			.println("********************* Data Load Option *************************** "
					+ loadOption);
			
			if(loadOption.contentEquals("db")){
			loadDataFromDB2();
			} else {
				
				//This loads the garment model from the XML
				DataBaseLoad();
				
			}			
			
			//We dont use this one loadDataFromDB();
			
			
			//dynamicImageController = new DynamicImageController();
			//dynamicImageController.setProductList(productList);
			//dynamicImageLoad();
			
			setupDone = true;
				}
	}*/
	
	

	public void DataBaseLoad() {

		CatalogJPA catalog = new CatalogJPA();

		catalog.setCatalogCode("Fall2013");
		

		catalog.setCreationDate(new Date());
		// moved this here
		entityManager.merge(catalog);
		// catalogService.create(catalog);

		// First we create it
		// catalogService.create(catalog);

		/*
		 * CategoryJPA category = new CategoryJPA();
		 * category.setCatalog(catalog);
		 * category.setCatalogId(catalog.getCatalogId()); //
		 * catalog.addCategory(category); catalogService.create(catalog);// we
		 * should check that update(catalog); // Do we need to implement just
		 * save to keep in memory catalogService.saveAndFlush(catalog);
		 * categoryService.create(category);
		 * categoryService.saveAndFlush(category);
		 */

		/**** Begining of the XML Load ***********/
		// moved this out of try
		CategoryJPA category = null;

		ProductJPA product = null;
		DescriptionJPA description = null;
		GalleryJPA gallery = null;
		String image = null;
		String imageDetails = null;
		String icon = null;
		File file = null;
		ProductReviewJPA review = null;
		// Resource resource =
		// appContext.getResource("classpath:com/mkyong/common/testing.txt");
		// try {
		Iterator it = fileList.iterator();
		while (it.hasNext()) {
			fileName = (String) it.next();
			System.out
					.println("********************* Now parsing *************************** "
							+ fileName);
			
			//FileSystemResource res = new FileSystemResource(new File("C:\\Users\\Tim\\fitlabsSpringCloud\\src\\main\\webapp\\resources\\images\\logomb_bub.png"));
			
			
			ClassPathResource classPathResource = new ClassPathResource("static/"+ fileName);
			
			
			
			org.springframework.core.io.Resource res2 = context.getResource("/WEB-INF/classes/" + fileName);
            //String pathToTemplate = res2.getURI().getPath();
            //System.out.println("PATH of resource =" + someResource);
            
			File descriptionFile = null;
			try {
				// descriptionFile = res2.getFile();
				descriptionFile = classPathResource.getFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//File descriptionFile = new File(
			//		"C:\\Users\\Tim\\fitlabsIntegPF4RC\\" + fileName);
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(descriptionFile);
				doc.getDocumentElement().normalize();

				// Read more:
				// http://javarevisited.blogspot.com/2011/12/parse-xml-file-in-java-example-tutorial.html#ixzz2NkGknhaW

				// ******
				// CategoryJPA category = new CategoryJPA();
				category = new CategoryJPA();
				category.setCategoryName(fileName);
				category.setCatalog(catalog);
				category.setCatalogId(catalog.getCatalogId());
				//
				catalog.addCategory(category);
				// This will be move out of the try and update here
				// catalogService.create(catalog)
				catalogService.update(catalog);// we should check that
												// update(catalog);

				// Do we need to implement just save to keep in memory
				catalogService.saveAndFlush(catalog);
				categoryService.create(category);
				categoryService.saveAndFlush(category);

				// ********

				NodeList listOfGarments = doc.getElementsByTagName("garment");
				int totalGarments = listOfGarments.getLength();
				System.out
						.println("Total number of Garments: " + totalGarments);

				for (int s = 0; s < listOfGarments.getLength(); s++) {

					product = new ProductJPA();
					product.setCategory(category);
					product.setCategoryId(category.getCategoryId());

					gallery = new GalleryJPA();

					// file = new File("resources/1finalLogoFitLabs.jpg");
					gallery.setMimeType("image/png");
					gallery.setProductId(product.getProductId());
					gallery.setProductJPA(product);

					description = new DescriptionJPA();
					description.setProductId(product.getProductId());
					description.setProductJPA(product);

					Node firstGarmentNode = listOfGarments.item(s);
					if (firstGarmentNode.getNodeType() == Node.ELEMENT_NODE) {

						Element firstGarmentElement = (Element) firstGarmentNode;

						// ------- Garment Description
						NodeList garmentDescriptionList = firstGarmentElement
								.getElementsByTagName("garment-description");
						Element firstDescriptionElement = (Element) garmentDescriptionList
								.item(0);

						NodeList textGarmentDescriptionList = firstDescriptionElement
								.getChildNodes();

						description
								.setProdDesc(((Node) textGarmentDescriptionList
										.item(0)).getNodeValue().trim());
						System.out.println("Garment DescriptionJPA: "
								+ ((Node) textGarmentDescriptionList.item(0))
										.getNodeValue().trim());

						// ------- fabric-content
						NodeList fabricContentDescriptionList = firstGarmentElement
								.getElementsByTagName("fabric-content");
						Element fabricContentDescriptionElement = (Element) fabricContentDescriptionList
								.item(0);

						NodeList textfabricContentDescriptionList = fabricContentDescriptionElement
								.getChildNodes();

						description
								.setFabricContent(((Node) textfabricContentDescriptionList
										.item(0)).getNodeValue().trim());
						System.out.println("FabricContent: "
								+ ((Node) textfabricContentDescriptionList
										.item(0)).getNodeValue().trim());

						// ------- fabric-properties
						NodeList fabricPropertiesDescriptionList = firstGarmentElement
								.getElementsByTagName("fabric-properties");
						Element fabricPropertiesDescriptionElement = (Element) fabricPropertiesDescriptionList
								.item(0);

						NodeList textfabricPropertiesDescriptionList = fabricPropertiesDescriptionElement
								.getChildNodes();

						description
								.setFabricProperties(((Node) textfabricPropertiesDescriptionList
										.item(0)).getNodeValue().trim());
						System.out.println("FabricProperties: "
								+ ((Node) textfabricPropertiesDescriptionList
										.item(0)).getNodeValue().trim());

						// ------- grament-construction
						NodeList gramentConstructionDescriptionList = firstGarmentElement
								.getElementsByTagName("grament-construction");
						Element gramentConstructionDescriptionElement = (Element) gramentConstructionDescriptionList
								.item(0);

						NodeList textgramentConstructionDescriptionList = gramentConstructionDescriptionElement
								.getChildNodes();

						description
								.setGramentConstruction(((Node) textgramentConstructionDescriptionList
										.item(0)).getNodeValue().trim());
						System.out
								.println("Garment Construction: "
										+ ((Node) textgramentConstructionDescriptionList
												.item(0)).getNodeValue().trim());

						// ------- design-highlights
						NodeList designHighlightsDescriptionList = firstGarmentElement
								.getElementsByTagName("design-highlights");
						Element designHighlightsDescriptionElement = (Element) designHighlightsDescriptionList
								.item(0);

						NodeList textdesignHighlightsDescriptionList = designHighlightsDescriptionElement
								.getChildNodes();

						description
								.setDesignHighlights(((Node) textdesignHighlightsDescriptionList
										.item(0)).getNodeValue().trim());
						System.out.println("DesignHighlights: "
								+ ((Node) textdesignHighlightsDescriptionList
										.item(0)).getNodeValue().trim());

						// ------- general-fit
						NodeList generalFitDescriptionList = firstGarmentElement
								.getElementsByTagName("general-fit");
						Element generalFitDescriptionElement = (Element) generalFitDescriptionList
								.item(0);

						NodeList textgeneralFitDescriptionList = generalFitDescriptionElement
								.getChildNodes();

						description
								.setGeneralFit(((Node) textgeneralFitDescriptionList
										.item(0)).getNodeValue().trim());
						System.out
								.println("General Fit: "
										+ ((Node) textgeneralFitDescriptionList
												.item(0)).getNodeValue().trim());

						// ------- general-length
						NodeList generalLengthDescriptionList = firstGarmentElement
								.getElementsByTagName("general-length");
						Element generalLengthDescriptionElement = (Element) generalLengthDescriptionList
								.item(0);

						NodeList textgeneralLengthDescriptionList = generalLengthDescriptionElement
								.getChildNodes();

						description
								.setGeneralLength(((Node) textgeneralLengthDescriptionList
										.item(0)).getNodeValue().trim());
						System.out.println("General Lenght: "
								+ ((Node) textgeneralLengthDescriptionList
										.item(0)).getNodeValue().trim());

						// ---- urls

						NodeList imageUrlList = firstGarmentElement
								.getElementsByTagName("image-url");
						Element firstImageUrlElement = (Element) imageUrlList
								.item(0);

						NodeList textImageUrlList = firstImageUrlElement
								.getChildNodes();

						gallery.setImageUrl(((Node) textImageUrlList.item(0))
								.getNodeValue().trim());

						System.out.println("ImageUrl : "
								+ ((Node) textImageUrlList.item(0))
										.getNodeValue().trim());

						// - img file name

						NodeList imgList = firstGarmentElement
								.getElementsByTagName("img");
						Element firstImgElement = (Element) imgList.item(0);

						NodeList textImgList = firstImgElement.getChildNodes();

						gallery.setImg(((Node) textImgList.item(0))
								.getNodeValue().trim());

						System.out.println("Img fileName : "
								+ ((Node) textImgList.item(0)).getNodeValue()
										.trim());

						// - imageList file names

						NodeList nodes = firstGarmentElement
								.getElementsByTagName("image");

						for (int x = 0; x < nodes.getLength(); x++) {
							Node node = nodes.item(x);
							if (node.getNodeName().equalsIgnoreCase("image")) {
								NodeList childNodes = node.getChildNodes();
								for (int y = 0; y < childNodes.getLength(); y++) {
									Node data = childNodes.item(y);
									if (data.getNodeType() == Node.TEXT_NODE)
										System.out.println("ImageList entry : "
												+ ((Node) childNodes.item(y))
														.getNodeValue().trim());

									image = ((Node) childNodes.item(y))
											.getNodeValue().trim();
									String image1 = new String(image);

									gallery.addNewImageItem(image1);

								}
							}
						}
						
						// Begin ImageDetails
						
						NodeList nodesImageDetails = firstGarmentElement
								.getElementsByTagName("imageDetails");

						for (int x = 0; x < nodesImageDetails.getLength(); x++) {
							Node node = nodesImageDetails.item(x);
							if (node.getNodeName().equalsIgnoreCase("imageDetails")) {
								NodeList childNodes = node.getChildNodes();
								for (int y = 0; y < childNodes.getLength(); y++) {
									Node data = childNodes.item(y);
									if (data.getNodeType() == Node.TEXT_NODE)
										System.out.println("ImageDetailsList entry : "
												+ ((Node) childNodes.item(y))
														.getNodeValue().trim());

									imageDetails = ((Node) childNodes.item(y))
											.getNodeValue().trim();
									String imageD = new String(imageDetails);

									gallery.addNewImageDetailItem(imageD);

								}
							}
						}
						
						// End ImageDetails

						// -IconList file names

						NodeList iconNodes = firstGarmentElement
								.getElementsByTagName("icon");

						for (int x = 0; x < iconNodes.getLength(); x++) {
							Node node = iconNodes.item(x);
							if (node.getNodeName().equalsIgnoreCase("icon")) {
								NodeList childNodes = node.getChildNodes();
								for (int y = 0; y < childNodes.getLength(); y++) {
									Node data = childNodes.item(y);
									if (data.getNodeType() == Node.TEXT_NODE)
										System.out.println("IconList entry : "
												+ ((Node) childNodes.item(y))
														.getNodeValue().trim());

									icon = ((Node) childNodes.item(y))
											.getNodeValue().trim();
									String icon1 = new String(icon);

									gallery.addNewIconItem(icon1);

								}
							}
						}

						// -- Garment-name

						NodeList nameList = firstGarmentElement
								.getElementsByTagName("garment-name");
						Element firstNameElement = (Element) nameList.item(0);

						NodeList textNameList = firstNameElement
								.getChildNodes();

						product.setName(((Node) textNameList.item(0))
								.getNodeValue().trim());

						System.out.println("Garment-name : "
								+ ((Node) textNameList.item(0)).getNodeValue()
										.trim());

						// -- Price

						NodeList priceList = firstGarmentElement
								.getElementsByTagName("product-price");
						Element firstPriceElement = (Element) priceList.item(0);

						NodeList textPriceList = firstPriceElement
								.getChildNodes();

						product.setPrice(((Node) textPriceList.item(0))
								.getNodeValue().trim());

						System.out.println("Product-price : "
								+ ((Node) textNameList.item(0)).getNodeValue()
										.trim());

						// --- Product-Code

						NodeList productCode = firstGarmentElement
								.getElementsByTagName("product-code");
						Element firstProductCodeElement = (Element) productCode
								.item(0);

						NodeList textProductCode = firstProductCodeElement
								.getChildNodes();

						product.setProductCode(((Node) textProductCode.item(0))
								.getNodeValue().trim());
						System.out.println("Product Code : "
								+ ((Node) textProductCode.item(0))
										.getNodeValue().trim());

						// -- Style-Number

						NodeList styleNumber = firstGarmentElement
								.getElementsByTagName("style-number");
						Element firstStyleNumberElement = (Element) styleNumber
								.item(0);

						NodeList textStyleNumber = firstStyleNumberElement
								.getChildNodes();

						product.setStyleNumber(((Node) textStyleNumber.item(0))
								.getNodeValue().trim());
						System.out.println("Style Number : "
								+ ((Node) textStyleNumber.item(0))
										.getNodeValue().trim());

						// -- Color originally NodeList colorList

						NodeList colorNodes = firstGarmentElement
								.getElementsByTagName("color");

						for (int x = 0; x < colorNodes.getLength(); x++) {
							Node node = colorNodes.item(x);
							if (node.getNodeName().equalsIgnoreCase("color")) {
								NodeList childNodes = node.getChildNodes();
								for (int y = 0; y < childNodes.getLength(); y++) {
									Node data = childNodes.item(y);
									if (data.getNodeType() == Node.TEXT_NODE)
										System.out.println("Color : "
												+ ((Node) childNodes.item(y))
														.getNodeValue().trim());

									String col = ((Node) childNodes.item(y))
											.getNodeValue().trim();
									String col1 = new String(col);
									
									product.addNewColorItem(col1);
									
								}
							}
						}

						// - Sizes
						NodeList sizenodes = firstGarmentElement
								.getElementsByTagName("size");

						for (int x = 0; x < sizenodes.getLength(); x++) {
							Node node = sizenodes.item(x);
							if (node.getNodeName().equalsIgnoreCase("size")) {
								NodeList childNodes = node.getChildNodes();
								for (int y = 0; y < childNodes.getLength(); y++) {
									Node data = childNodes.item(y);
									if (data.getNodeType() == Node.TEXT_NODE)
										System.out.println("Size : "
												+ ((Node) childNodes.item(y))
														.getNodeValue().trim());

									String siz = ((Node) childNodes.item(y))
											.getNodeValue().trim();
									String siz1 = new String(siz);
									
									product.addNewSize(siz1);
									
								}
							}
						}

					}// end of if clause

					// Set the Product on description since we have got all the
					// values
					product.setDescription(description);
					// Set the Product on gallery since we have got all the
					// values
					product.setGallery(gallery);
					// Build the iconToImageMap
					gallery.buildIconToPicMap();

					// - Add Reviews Since values dont come from xml
					/*
					 * review = new ProductReviewJPA(); String prodreview =
					 * "Damn Nice Product"; review.setReview(prodreview);
					 * Integer val = new Integer(3); review.setRating(val);
					 * review.setProductId(product.getProductId());
					 * review.setProductJPA(product);
					 */

					review = new ProductReviewJPA();
					String prodreview = "Damn Nice Product High-performance meets style in these versatile work-out bottoms. "
							+ "Offered in a short, capri, and full-length pant, these garments"
							+ "flatter with a semi-fitted leg, vertical color accent lines and"
							+ "strategic contrast-color splashes. Pull on style with wide elastic"
							+ "waist and drawstring for perfect fit. All styles have a convenient"
							+ "thigh pocket, large enough for your phone or keys. Medium weight"
							+ "4-way stretch poly-lycra blend fabric offers breathability and flattering fit.";
					review.setReview(prodreview);
					review.setCreationDate(new Date());
					review.setAgeDemo(35);
					review.setReviewOwner("tseckend@fit-labs.com");
					review.setDisplayName("tseckend");
					Integer val = new Integer(3);
					review.setRating(val);
					review.setProductId(product.getProductId());
					review.setProductJPA(product);

					product.addProductReview(review);

					// First we persist the Product so the DB will not complian
					// about not
					// knowing productId
					productService.create(product);
					productService.saveAndFlush(product);

					descriptionService.create(description);
					descriptionService.saveAndFlush(description);

					productReviewService.create(review);
					productReviewService.saveAndFlush(review);

					category.addProduct(product);

					category.setCreationDate(new Date());
					// category.setCategoryName("Athletic");

					categoryService.update(category);
					categoryService.saveAndFlush(category);

					// catalogService.update(catalog);// we should check that
					// update(catalog);
					entityManager.merge(catalog); // works
					catalogService.saveAndFlush(catalog);

				}// end of for loop with s var
				// } // end of for with fileName iterateor

			} catch (SAXParseException err) {
				System.out.println("** Parsing error" + ", line "
						+ err.getLineNumber() + ", uri " + err.getSystemId());
				System.out.println(" " + err.getMessage());

			} catch (SAXException e) {
				Exception x = e.getException();
				((x == null) ? e : x).printStackTrace();

			} catch (Throwable t) {
				t.printStackTrace();
			}

			// All this is out side the try and for loop so it will only
			// retrieve the last category
			CategoryJPA caty = categoryService.findByCategoryName(category.getCategoryName());//   findByCategoryId(category.getCategoryName());
			//CategoryJPA caty = categoryService.findByCategoryId(category.getCategoryName());//	.getCategoryId());
			CatalogJPA cat = catalogService.findByCatalogId(catalog
					.getCatalogId());// findByName("TimsTest1");

			// ************ All These Review objects come back as null in the UI
			// so we use
			// ** Servlet 3.0 functionality to do an
			// value="#{garmentModel.selectedProduct.reviews.toArray()}" to call
			// the object directly

			for (Iterator<IProduct> testProds = caty.getProducts().iterator(); testProds
					.hasNext();) {
				Object obj = testProds.next();
				ProductJPA e = (ProductJPA) obj;
				Collection<IProductReview> reviews = e.getReviews();
				if (reviews.isEmpty()) {
					System.out
							.println("******Prods collection from Cat1 reviews are empty in Product *********** :");
					// productList.add(e);
				} else {

					ApplicationDataBaseLoadApi.testCatList.add(e);
					for (Iterator<IProductReview> testRev = reviews.iterator(); testRev
							.hasNext();) {

						ProductReviewJPA pr = (ProductReviewJPA) testRev.next();

						String rev = (String) pr.getReview();

						System.out.println("The Product Review :"
								+ pr.getReview().toString());
						System.out.println("The Product Rating :"
								+ pr.getRating());
					}

				}

			}

			@SuppressWarnings("unused")
			String catName = cat.getCatalogCode();

			Set<ICategory> cats = new HashSet<ICategory>();

			Set<IProduct> prods = new HashSet<IProduct>();

			// ApplicationDataBaseLoadApi.productList = new
			// ArrayList<ProductJPA>();

			cats.addAll((Collection<? extends ICategory>) cat.getCategories());

			for (Iterator<ICategory> catIt = cats.iterator(); catIt.hasNext();) {

				Object o = catIt.next();

				CategoryJPA cat1 = (CategoryJPA) o;
				System.out.println("************* Category Name **********"
						+ cat1.getCategoryName());
				prods.addAll((Collection<? extends IProduct>) cat1
						.getProducts());

				for (Iterator<IProduct> itOverProds = prods.iterator(); itOverProds
						.hasNext();) {
					Object obj = itOverProds.next();
					ProductJPA e = (ProductJPA) obj;
					productList.add(e);

				}

				for (Iterator<ProductJPA> itOverPList = productList.iterator(); itOverPList
						.hasNext();) {
					ProductJPA p = itOverPList.next();
					System.out.println("Product values CategoryId :"
							+ p.getCategoryId());
					System.out.println("Product values ProductName :"
							+ p.getName());
					System.out.println("Product values ProductPrice :"
							+ p.getPrice());
					System.out.println("Product values ProductCode :"
							+ p.getProductCode());
					System.out.println("Product values ProductId :"
							+ p.getProductId());

					Collection<String> c = p.getColors();

					for (Iterator<String> itOverC = c.iterator(); itOverC
							.hasNext();) {

						Object colorString = itOverC.next();

						String colorVal = (String) colorString;

						System.out.println("Color values :" + colorVal);
					}

					System.out.println("Product values Description :"
							+ p.getDescription().getProdDesc());
					System.out.println("Product values ImageUrl :"
							+ p.getGallery().getImageUrl());
					System.out.println("Product values Img FileName :"
							+ p.getGallery().getImg());

					Collection<String> iconList = p.getGallery().getIconList();
					for (Iterator<String> itOverIcons = iconList.iterator(); itOverIcons
							.hasNext();) {

						System.out.println("IconList values :"
								+ itOverIcons.next());
					}

					Collection<String> imageList = p.getGallery()
							.getImageList();
					for (Iterator<String> itOverImage = imageList.iterator(); itOverImage
							.hasNext();) {

						System.out.println("ImageList values :"
								+ itOverImage.next());
					}

					Collection<String> siz = p.getSizes();
					for (Iterator<String> itOverSiz = siz.iterator(); itOverSiz
							.hasNext();) {

						System.out.println("Sizes values :" + itOverSiz.next());
					}

					// Collection<IProductReview> reviews = p.getReviews();
					Set<IProductReview> reviews = p.getReviews();
					for (Iterator<IProductReview> itOverRev = reviews
							.iterator(); itOverRev.hasNext();) {

						ProductReviewJPA pr = (ProductReviewJPA) itOverRev
								.next();

						String rev = (String) pr.getReview();

						System.out.println("The Product Review :"
								+ pr.getReview().toString());
						System.out.println("The Product Rating :"
								+ pr.getRating());
					}

				}

			}
		}
	}
	
	public void loadDataFromDB2() {
		
		Iterator categoryIt = categoryList.iterator();
		while (categoryIt.hasNext()) {
			categoryName = (String) categoryIt.next();
			System.out
					.println("********************* Now Loading *************************** "
							+ categoryName);
			
			
			

			CategoryJPA oneCat = categoryService.findByCategoryName(categoryName);
			//System.out.println("Got the catalog: " + oneCat.getCategoryName());
			
			@SuppressWarnings("unused")
			//String catName = cat.getCatalogCode();

			Set<ICategory> cats = new HashSet<ICategory>();

			Set<IProduct> prods = new HashSet<IProduct>();

			System.out.println("************* Category Name **********"+oneCat.getCategoryName());
				if(oneCat.getCategoryName().equalsIgnoreCase("AthleticTopsDescNov19.xml") )
					
					
					
				prods.addAll((Collection<? extends IProduct>) oneCat.getProducts());

				for (Iterator<IProduct> itOverProds = prods.iterator(); itOverProds.hasNext();) {
					Object obj = itOverProds.next();
					ProductJPA e = (ProductJPA) obj;
					Collection<IProductReview> reviews = e.getReviews();
					if(reviews.isEmpty()){
						System.out.println("******Prods collection from Cat1 reviews are empty in Product *********** :");
						//productList.add(e);
					} else {
						
						athleticTopsCatList.add(e);
						//productList.add(e);
							for (Iterator<IProductReview> testRev = reviews.iterator(); testRev.hasNext();){
							
							ProductReviewJPA pr = (ProductReviewJPA)testRev.next();
							
							String rev = (String) pr.getReview();
							
							System.out.println("The Product Review :"+pr.getReview().toString());
							System.out.println("The Product Rating :"+pr.getRating());
							}
					}
				} 
				
				if (oneCat.getCategoryName().equalsIgnoreCase("AthleticBottomsDescNov19.xml")) {
					
					prods.addAll((Collection<? extends IProduct>) oneCat.getProducts());

					for (Iterator<IProduct> itOverProds = prods.iterator(); itOverProds.hasNext();) {
						Object obj = itOverProds.next();
						ProductJPA e = (ProductJPA) obj;
						Collection<IProductReview> reviews = e.getReviews();
						if(reviews.isEmpty()){
							System.out.println("******Prods collection from Cat1 reviews are empty in Product *********** :");
							//productList.add(e);
						} else {
							
							athleticBottomsCatList.add(e);
							//productList.add(e);
								for (Iterator<IProductReview> testRev = reviews.iterator(); testRev.hasNext();){
								
								ProductReviewJPA pr = (ProductReviewJPA)testRev.next();
								
								String rev = (String) pr.getReview();
								
								System.out.println("The Product Review :"+pr.getReview().toString());
								System.out.println("The Product Rating :"+pr.getRating());
								}
						}
					}
					
					
					
				}
				
		}

	}
		
		
		
		
		
	
	
	public void loadDataFromDB(){
				
		CategoryJPA oneCat = categoryService.findByCategoryName("AthleticTopsDescNov19.xml");
		//System.out.println("Got the catalog: " + oneCat.getCategoryName());
		
		@SuppressWarnings("unused")
		//String catName = cat.getCatalogCode();

		Set<ICategory> cats = new HashSet<ICategory>();

		Set<IProduct> prods = new HashSet<IProduct>();

		System.out.println("************* Category Name **********"+oneCat.getCategoryName());
			
			prods.addAll((Collection<? extends IProduct>) oneCat.getProducts());

			for (Iterator<IProduct> itOverProds = prods.iterator(); itOverProds.hasNext();) {
				Object obj = itOverProds.next();
				ProductJPA e = (ProductJPA) obj;
				Collection<IProductReview> reviews = e.getReviews();
				if(reviews.isEmpty()){
					System.out.println("******Prods collection from Cat1 reviews are empty in Product *********** :");
					//productList.add(e);
				} else {
					
					testCatList.add(e);
					//productList.add(e);
						for (Iterator<IProductReview> testRev = reviews.iterator(); testRev.hasNext();){
						
						ProductReviewJPA pr = (ProductReviewJPA)testRev.next();
						
						String rev = (String) pr.getReview();
						
						System.out.println("The Product Review :"+pr.getReview().toString());
						System.out.println("The Product Rating :"+pr.getRating());
						}
				}
			}
				

			
		
	}
	
	
	public byte[] readImageOldWay(File file) throws IOException
	{
	  //Logger.getLogger(Main.class.getName()).log(Level.INFO, "[Open File] " + file.getAbsolutePath());
	  InputStream is = new FileInputStream(file);
	  // Get the size of the file
	  long length = file.length();
	  // You cannot create an array using a long type.
	  // It needs to be an int type.
	  // Before converting to an int type, check
	  // to ensure that file is not larger than Integer.MAX_VALUE.
	  if (length > Integer.MAX_VALUE)
	  {
	    // File is too large
	  }
	  // Create the byte array to hold the data
	  byte[] bytes = new byte[(int) length];
	  // Read in the bytes
	  int offset = 0;
	  int numRead = 0;
	  while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)
	  {
	    offset += numRead;
	  }
	  // Ensure all the bytes have been read in
	  if (offset < bytes.length)
	  {
	    throw new IOException("Could not completely read file " + file.getName());
	  }
	  // Close the input stream and return bytes
	  is.close();
	  return bytes;
	}
	
	
	
	
	

	
	
	
	
    
    
      
    
}  
