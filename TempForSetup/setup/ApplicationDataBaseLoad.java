package com.timbre.spring.persistence.setup;

import java.util.ArrayList;

import com.timbre.sec.model.products.ProductJPA;

public interface ApplicationDataBaseLoad {
	
	public ArrayList<ProductJPA> getProductList();
	
	public ArrayList<ProductJPA> getTestCatList();

	public ArrayList<ProductJPA> getAthleticBottomsCatList();
	
	public ArrayList<ProductJPA> getAthleticTopsCatList();

}
