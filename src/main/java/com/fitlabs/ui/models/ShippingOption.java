package com.fitlabs.ui.models;

public class ShippingOption{
	
	private String providerType;
	private String duration;
	private String cost;
	
	public String standardTypeCost;
	public String expressTypeCost;
	public String overNightTypeCost;
	
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getDuration() {
		return duration;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getCost() {
		return cost;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public String getProviderType() {
		return providerType;
	}
	public void setStandardTypeCost(String standardTypeCost) {
		this.standardTypeCost = standardTypeCost;
	}
	public String getStandardTypeCost() {
		return standardTypeCost;
	}
	public void setExpressTypeCost(String expressTypeCost) {
		this.expressTypeCost = expressTypeCost;
	}
	public String getExpressTypeCost() {
		return expressTypeCost;
	}
	public void setOverNightTypeCost(String overNightTypeCost) {
		this.overNightTypeCost = overNightTypeCost;
	}
	public String getOverNightTypeCost() {
		return overNightTypeCost;
	}
}
