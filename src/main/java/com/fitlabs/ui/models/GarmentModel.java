/*package com.fitlabs.ui.models;


import java.awt.image.BufferedImage;
import java.io.IOException;  
import java.io.Serializable;  
import java.util.ArrayList;  
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;  
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;  
import javax.faces.event.ActionEvent;  
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.PhaseId;
import javax.faces.view.Location;
import javax.persistence.Transient;
import javax.servlet.ServletContext;  
  


import org.primefaces.component.log.Log;
import org.primefaces.event.RateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.jsf.FacesContextUtils;




import com.timbre.sec.model.products.ProductJPA;
import com.timbre.spring.persistence.setup.ApplicationDataBaseLoadApi;
import com.timbre.spring.persistence.setup.ApplicationDataBaseLoad;

//import org.primefaces.event.RateEvent; 


//@ManagedBean(name="garmentModel")
@Scope("session")


public class GarmentModel implements Serializable {
	
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

	//private final static String[] colors;
	
	@Autowired 
	ApplicationContext context;
    
	//@Autowired //(name="contentLoader")
	ApplicationDataBaseLoad loader;
    
    public ApplicationDataBaseLoad getLoader() {
		return loader;
	}

	public void setLoader(ApplicationDataBaseLoad loader) {
		this.loader = loader;
	}

	public static final int MAX_IMAGE_COUNT = 1;
    private static final Logger logger = LoggerFactory.getLogger(GarmentModel.class);
    
    public ArrayList<Photo> imageList;
    public ArrayList<Photo> imageListAthleticBottoms;
    public ArrayList<Photo> imageListBrasWarmups;
    
    public ArrayList<Photo> imageListActiveTops;
    public ArrayList<Photo> imageListActiveBottoms;
    public ArrayList<Photo> imageListActiveBrasWarmups;
    public ArrayList<ProductJPA> garmentList;// = new ArrayList<ProductJPA>();
    public ArrayList<ProductJPA> athleticTopsList;
    

	public ArrayList<ProductJPA> athleticBottomsList;
    
	public ArrayList<ProductJPA> getAthleticTopsList() {
		loader = (ApplicationDataBaseLoadApi) findBean("contentLoader");
		return (ArrayList<ProductJPA>) loader.getAthleticTopsCatList();
	}

	public void setAthleticTopsList(ArrayList<ProductJPA> athleticTopsList) {
		this.athleticTopsList = athleticTopsList;
	}
	
    public ArrayList<ProductJPA> getAthleticBottomsList() {
    	
    	loader = (ApplicationDataBaseLoadApi) findBean("contentLoader");
    	return (ArrayList<ProductJPA>) loader.getAthleticBottomsCatList();
	}

	public void setAthleticBottomsList(ArrayList<ProductJPA> athleticBottomsList) {
		this.athleticBottomsList = athleticBottomsList;
	}

	public ArrayList<ProductJPA> getGarmentList() {
    	    	
    	if(garmentList == null || garmentList.isEmpty()){
    		
    		loader = (ApplicationDataBaseLoadApi) findBean("contentLoader");
    	  	garmentList = (ArrayList<ProductJPA>) loader.getTestCatList();
    	return garmentList ;
    	} 
    	
    	return garmentList;
	}
    
      
    public  String quantity[] = new String[]{"1", "2","3", "4", "5"};  
    
    
    public  String[] getQuantity() {
		return quantity;
    }
    
    public  String overallRating[] = new String[]{"1", "2","3", "4", "5"};
    
    public String[] getOverallRating(){
    	return overallRating;
    }
    

	public void setGarmentList(ArrayList<ProductJPA> garmentList) {
		this.garmentList = garmentList;
	}

	BufferedImage image;
    public String imageURL;
    public String name = null;
    public Photo selectedPhoto;// = new Photo();
    
    *//** Used to select the current productJPA in the productList<ProductJPA> *//*
    public ProductJPA selectedProduct;
    public ProductJPA tempProduct;
    
    public String selectedPhotoName = null;
    
    public String iconName;
    public String selectedIconPic;
    private Map<String,String> iconsAndPics = new HashMap<String, String>(); 
    
    
    public String outcome(){
    	FacesContext context = FacesContext.getCurrentInstance();
    	if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            //return new DefaultStreamedContent();
    		System.out.print("We are in Render_Response: ");
    		return "result";
        }
        else {
            // So, browser is requesting the image. Get ID value from actual request param.
            //String id = context.getExternalContext().getRequestParameterMap().get("id");
            //Image image = service.find(Long.valueOf(id));
            //return new DefaultStreamedContent(new ByteArrayInputStream(image.getBytes()));
        	this.iconName = getIconParam(context);
        	 
    		return "result";
        }
    	
		
	}
    
	public ProductJPA getSelectedProduct() {
		
		if(selectedProduct == null){
			return tempProduct;
		}
		
		
		return selectedProduct;
	}

	public void setSelectedProduct(ProductJPA selectedProduct) {
		this.selectedProduct = selectedProduct;
		this.tempProduct = selectedProduct;
		logger.info("The selected Product on product grid :" +selectedProduct.getName());
		//System.out.println("The selected Product on landingPage2 product grid :" +selectedProduct.getName());
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	
	
	public String filterIcon(AjaxBehaviorEvent event) {
	    String filterValue = (String) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("filterIcon");
	    System.out.println("filter string :  with query filter of : " + filterValue);
	    
	    System.out.println("icon value change in gallery value :"+event.getSource().toString());
		//event.getComponent().getAttributes().values();
		
		for (Iterator<Object> it = event.getComponent().getAttributes().values().iterator (); it.hasNext (); ) {
				Object o = it.next ();
				Location value =(javax.faces.view.Location)o;
				
				
					System.out.println("filterIcon event.getComponent.getAttributes.values :"+value);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					
			}


	    return filterValue;
	}

	//get value from "f:param"
	public String getIconParam(FacesContext fc){
 
		String n = fc.getExternalContext().getRequestParameterMap().get("id");
		System.out.print(n + ": ");
		Set set = params.entrySet(); 
		Iterator i = set.iterator();
		// Display elements
		while(i.hasNext()) {
		Map.Entry me = (Map.Entry)i.next();
		System.out.print(me.getKey() + ": ");
		System.out.println(me.getValue());
		//} 
		
		
		//String icn = params.get("iconName");
		
		logger.debug("icn in params is :", icn);
		logger.debug("iconName in params is :", params.get("iconName"));
		logger.debug("Pics name retrieved from Map :", iconsAndPics.get("iconName"));
		
		
		
	       
	         return   iconsAndPics.get(iconName);
	        //return  selectedIconPic;
	          
	     
 
	}  
    
    
    
    
    public String getSelectedPhotoName() {
		return selectedPhotoName;
	}

	public void setSelectedPhotoName(String selectedPhotoName) {
		this.selectedPhotoName = selectedPhotoName;
	}

	//public String[] listOfNames = {"flgarment1.jpg", "flgarment2.jpg", "flgarment3.jpg", "flgarment22.jpg", "flgarment27.jpg", "flgarment28.jpg"};
    public int numberOfElements;
      
   
    public String[] listOfNames = {"raspberryRaglan.gif",
    								"bambooRaglanBabyPink2.gif",
    								"athleticTank.gif",
    								"blackBambooTurkBraTop.gif",
    								"sweatheartTank.gif",
    								"sweatheartSS.gif",
    								"bambooTankTop.gif",
    								"sweatheartLS.gif"};
    
    public String[] listOfNames = {
    		"FinalFeb9-004.png",
			"FinalFeb9-010.png",
			"FinalFeb9-019.png",
			"FinalFeb9-023.png",
			"FinalFeb9-026.png",
			"FinalFeb9-029.png",
			"FinalFeb9-037.png",
			"FinalFeb9-039.png",
			"FinalFeb9-047.png",
			"FinalFeb9-049.png",
			"FinalFeb9-052.png"
			};
    
  
    
    public String[] listOfAthleticBottoms = {
			"FinalFeb9-062.png",
			"FinalFeb9-069.png",
			"FinalFeb9-072.png",
			"FinalFeb9-076.png",
			"FinalFeb9-081.png",
    		"FinalFeb9-085.png",
			"FitSessionFeb152013-003.png"};
    
    
    
    public String[] listOfBrasWarmUps = {"FinalFeb9-055.png",
			"FinalFeb9-058.png",
			"sportsBra.gif",
			"sportsBraBack.gif"
			};
    
    public String[] listOfActiveTops = {
    		"NaturalFiberFeb11-017.png",
			"NaturalFiberFeb11-031.png",
			"NaturalFiberFeb11-041.png",
			"NaturalFiberFeb11-053.png",
			"NaturalFiberFeb11-059.png",
			"blackBambooBraTopTurk300x400.png"
			 };
    
    public String[] listOfActiveBottoms = {
    		"NaturalFiberFeb11-001.png",
    		"blackLeggingsSide.gif",
			"FitSessionFeb152013-009.png"
			 };
    
    public String[] listOfActiveBrasWarmUps = {
    		"FitSessionFeb152013-013.png",
    		"warmUpJacket.gif"
			 };
    
    
    
      
    public ArrayList<Photo> getImageListActiveBottoms() {
		return imageListActiveBottoms;
	}

	public void setImageListActiveBottoms(ArrayList<Photo> imageListActiveBottoms) {
		this.imageListActiveBottoms = imageListActiveBottoms;
	}

	public ArrayList<Photo> getImageListActiveBrasWarmups() {
		return imageListActiveBrasWarmups;
	}

	public void setImageListActiveBrasWarmups(
			ArrayList<Photo> imageListActiveBrasWarmups) {
		this.imageListActiveBrasWarmups = imageListActiveBrasWarmups;
	}

	 
  
    public GarmentModel() {
    	
    	//buildModels();
    	
    	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();//.getContext()
		ApplicationDataBaseLoadApi loader = (ApplicationDataBaseLoadApi) ec.getSessionMap().get("contentLoader");
		this.garmentList = (ArrayList<ProductJPA>) loader.getProductList();
		System.out.println("The number of products in garmentList :"+garmentList.size());
    	//ApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
    	
         
    }
    
    @PostConstruct
    public void buildModels(){
    	
    	    	
    	loader = (ApplicationDataBaseLoadApi) findBean("contentLoader");
    	//garmentList = (ArrayList<ProductJPA>) loader.getProductList();
    	garmentList = (ArrayList<ProductJPA>) loader.getTestCatList();
		//System.out.println("The number of products in garmentList :"+garmentList.size());
		
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T findBean(String beanName) {
	    FacesContext context = FacesContext.getCurrentInstance();
	    return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
	}
      
    public Photo getSelectedPhoto() {  
        return selectedPhoto;  
    }  
  
    public void setSelectedPhoto(Photo selectedPhoto) {
    	//logger.info("The name in Photo was ", selectedPhoto.name);
        this.selectedPhoto = selectedPhoto;
               
    }  
      
  
    public List<Photo> getImageListAthleticBottoms() {  
        return imageListAthleticBottoms;  
    } 
    
    public void setImageListAthleticBottoms(ArrayList<Photo> imageListAthleticBottoms)
    {
     this.imageListAthleticBottoms = imageListAthleticBottoms;
    }
    
    public List<Photo> getImageList() {  
        return imageList;  
    } 
    
    public void setImageList(ArrayList<Photo> imageList)
    {
     this.imageList = imageList;
    }
    
    public List<Photo> getImageListBrasWarmups() {  
        return imageListBrasWarmups;  
    } 
    
    public void setImageListBrasWarmups(ArrayList<Photo> imageListBrasWarmups)
    {
     this.imageListBrasWarmups = imageListBrasWarmups;
    }
    
    public ArrayList<Photo> getImageListActiveTops() {
		return imageListActiveTops;
	}

	public void setImageListActiveTops(ArrayList<Photo> imageListActiveTops) {
		this.imageListActiveTops = imageListActiveTops;
	}

	public int getNumberOfElements() {
    	return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
    	this.numberOfElements = numberOfElements;
    }
    
    public void setImageURL(String flURL) {
		this.imageURL = flURL;
		
	}

	public String getImageURL() {
		return imageURL;
		
	}
	
	

}
*/