package com.fitlabs.ui.models;

import java.io.Serializable;

public class GarmentSize implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String dressSize;
	private String bustSize;
	private String waistSize;
	private String hipSize;
	private String flSize;
	
	
	public String getFLSize() {
		return flSize;
	}
	public void setFLSize(String flSize) {
		this.flSize = flSize;
	}
	public String getHipSize() {
		return hipSize;
	}
	public void setHipSize(String hipSize) {
		this.hipSize = hipSize;
	}
	public String getWaistSize() {
		return waistSize;
	}
	public void setWaistSize(String waistSize) {
		this.waistSize = waistSize;
	}
	public String getBustSize() {
		return bustSize;
	}
	public void setBustSize(String bustSize) {
		this.bustSize = bustSize;
	}
	public String getDressSize() {
		return dressSize;
	}
	public void setDressSize(String dressSize) {
		this.dressSize = dressSize;
	}
	

}
