/*package com.fitlabs.ui.models;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;  

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.ListDataModel;  
  





import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.SelectableDataModel;  

import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.model.shopping.api.CartItem;
import com.timbre.sec.model.shopping.impl.CartItemJPA;

@SessionScoped 
public class ShoppingDataModel extends ListDataModel<CartItem> implements Serializable, SelectableDataModel<CartItem> {    
  
	
	public ProductJPA product;
	private CartItem selectedItem;
	private boolean discountsApply = false;
	private double total;
	
    public ShoppingDataModel() {  
    }  
  
    public ShoppingDataModel(List<CartItem> data) {  
        super(data);  
    }  
      
    @SuppressWarnings("unchecked")
	@Override  
    public CartItem getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<CartItem> items = (List<CartItem>) getWrappedData();  
          
        for(CartItem item : items) {  
            if(item.getItemCode().equals(rowKey))  
                return item;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(CartItem item) {  
        return item.getItemCode();  
    } 
    
    @SuppressWarnings("unchecked")
	public void add(CartItem item) {
        // dao.create(item);
        // Actually, the DAO should already have set the ID from DB. This is just for demo.
        item.setId(list.isEmpty() ? 1 : list.get(list.size() - 1).getId() + 1);
        list.add(item);
        item = new CartItemJPA(); // Reset placeholder.
        
    	List<CartItem> items = (List<CartItem>) getWrappedData();
    	items.add(item);
    }
    
    
    public void removeAllItems(){
    	
    	List<CartItemJPA> items = (List<CartItemJPA>) getWrappedData();
    	items.clear();
    	
    }
    
    
    @SuppressWarnings("unchecked")
	public void removeItem(ActionEvent actionEvent){
    	//System.out.println("We are in ShoppingDataModel.remove:");
    	List<CartItemJPA> items = (List<CartItemJPA>) getWrappedData();
    	
    	if(selectedItems == null)
        	return ;
        
        for(CartItem it: selectedItems){
        	//System.out.println("Values in selectedItems are :" +it.getItemCode());
        	
        	boolean itemIsIn = items.contains(it);
        	
        	//System.out.println("The testItem in the array is in the listArray :" +itemIsIn);
        	
        	if(itemIsIn){
        		items.remove(it);
        		doEstimateTotal();
        		
        		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item removed", it.getItemName());  
        		  
                FacesContext.getCurrentInstance().addMessage(null, msg);
        		for(int x=0; x<=selectedItems.length;x++){
        			
        		}
        	}
            
            for(CartItemJPA item : items) {  
                if(item.getItemCode().equals(rowKey))  
                    return item;  
            } 
        	
        	
        }
    	//System.out.println("The value of Item.code is :"+item.getItemCode());
    	//List<CartItem> items = (List<CartItem>) getWrappedData();
    	//items.remove(item);
    	
    }
    
    @SuppressWarnings("unchecked")
	public void removeMobleShoppingItems(ActionEvent actionEvent){
    	//System.out.println("We are in ShoppingDataModel.remove:");
    	List<CartItemJPA> items = (List<CartItemJPA>) getWrappedData();
    	
    	if(selectedItems == null)
        	return ;
        
        for(CartItem it: selectedItems){
        	//System.out.println("Values in selectedItems are :" +it.getItemCode());
        	
        	boolean itemIsIn = items.contains(it);
        	
        	//System.out.println("The testItem in the array is in the listArray :" +itemIsIn);
        	
        	if(itemIsIn){
        		items.remove(it);
        		doEstimateTotal();
        		
        		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item removed", it.getItemName());  
        		  
                FacesContext.getCurrentInstance().addMessage(null, msg);
        		for(int x=0; x<=selectedItems.length;x++){
        			
        		}
        	}
            
            for(CartItemJPA item : items) {  
                if(item.getItemCode().equals(getRowKey(item)))  
                	items.remove(item); 
            } 
    	items.clear();
    		
        	
        	
        }
    	
    
    @SuppressWarnings("unchecked")
	public void removeItem(AjaxBehaviorEvent abEvent){
    	//System.out.println("We are in ShoppingDataModel.remove:");
    	List<CartItemJPA> items = (List<CartItemJPA>) getWrappedData();
    	
    	if(selectedItems == null)
        	return ;
        
        for(CartItem it: selectedItems){
        	//System.out.println("Values in selectedItems are :" +it.getItemCode());
        	
        	boolean itemIsIn = items.contains(it);
        	
        	//System.out.println("The testItem in the array is in the listArray :" +itemIsIn);
        	
        	if(itemIsIn){
        		items.remove(it);
        		doEstimateTotal();
        		
        		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item removed", it.getItemName());  
        		  
                FacesContext.getCurrentInstance().addMessage(null, msg);
        		for(int x=0; x<=selectedItems.length;x++){
        			
        		}
        	}
            
            for(CartItemJPA item : items) {  
                if(item.getItemCode().equals(rowKey))  
                    return item;  
            } 
        	
        	
        }
    	//System.out.println("The value of Item.code is :"+item.getItemCode());
    	//List<CartItem> items = (List<CartItem>) getWrappedData();
    	//items.remove(item);
    	
    }
    
    public boolean isEmpty(){
    	
    	List<CartItem> items = (List<CartItem>) getWrappedData();
    	
    	return items.isEmpty();
    }
    
    public CartItem selectedItems[];

    public CartItem[] getSelectedItems() {
    	
    	
        //System.out.println("We are in getSelectedItems");
        if(selectedItems == null)
        	return selectedItems;
        
        for(CartItem it: selectedItems){
        	//System.out.println("ItemCode Values in selectedItems are :" +it.getItemCode());
        }
    	return selectedItems;
    }

    public void setSelectedItems(CartItem selectedItems[]) {
        this.selectedItems = selectedItems;
    }
	public void check(SelectEvent event) {
        System.out.println("in check");
    }
	
	public double doEstimateTotal(){
		
		//apply any discounts
	
		//double ammount = 0;
		total = 0.0;
		List<CartItem> items = (List<CartItem>) getWrappedData();
		
		if(discountsApply){
	
	    for (Iterator it = items.iterator(); it.hasNext();) {
	    	CartItemJPA item = (CartItemJPA) it.next();
	        double unitCost = item.getPerItemCost();
	         
	        total += (unitCost * item.getQty() * (100 - item.getDiscount() / 100 ));
	    }
	
	    return total;
	    
		} else {
			
			for (Iterator it = items.iterator(); it.hasNext();) {
		    	CartItem item = (CartItem) it.next();
		        double unitCost = item.getPerItemCost();
		        total += (unitCost * item.getQty());
		    }
			System.out.println("totals from ShoppingDataModel: "+total);
		    return total;
			
		}
	}
	
	public CartItem getSelectedCartItem() {  
        return selectedItem;  
    }  
    public void setSelectedCartItem(CartItem selectedItem) {  
        this.selectedItem = selectedItem;  
    }  
  
    public ShoppingDataModel getShoppingDataModel() {  
        return this;  
    }  
  
    public void onRowSelect(SelectEvent event) { 
    	
    	this.selectedItem = (CartItem) event.getObject();
    	
    	//System.out.println("We are in onRowSelect");
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Selected", ((CartItem) event.getObject()).getColor());  
  
        //FacesContext.getCurrentInstance().addMessage(null, msg);  
    }  
  
    public void onRowUnselect(UnselectEvent event) {
    	//System.out.println("We are in onRowUnselect");
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected", ((CartItem) event.getObject()).getColor());  
  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }
	
	public void onEdit(RowEditEvent event) {  
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Edited", ((CartItem) event.getObject()).getColor());  
  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    } 
	
	public void onCancel(RowEditEvent event) {  
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item edit Cancelled", ((CartItem) event.getObject()).getColor());  
  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }

	public void setSelectedItem(CartItem selectedItem) {
		//System.out.println("We are in setSelectedItem");
		this.selectedItem = selectedItem;
	}

	public CartItem getSelectedItem() {
		//System.out.println("We are in getSelectedItem");
		return selectedItem;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public ProductJPA getProduct() {
		return product;
	}

	public void setProduct(ProductJPA product) {
		this.product = product;
	}
	
	
    
    
    
}  
*/