/*package com.fitlabs.ui.models;

import java.awt.Graphics2D;  
import java.awt.image.BufferedImage;  
import java.io.ByteArrayInputStream;  
import java.io.ByteArrayOutputStream;  
import java.io.File;  
import java.io.FileInputStream;  
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.imageio.ImageIO;  
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
  
import net.sourceforge.barbecue.BarcodeFactory;  
import net.sourceforge.barbecue.BarcodeImageHandler;  
  
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;  
import org.jfree.chart.ChartUtilities;  
import org.jfree.chart.JFreeChart;  
import org.jfree.data.general.DefaultPieDataset;  
import org.jfree.data.general.PieDataset;  
import org.primefaces.model.DefaultStreamedContent;  
import org.primefaces.model.StreamedContent;  
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.fitlabs.util.XMLUtility;
import com.timbre.persistence.jpa.model.EntityFactoryJPA;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.persistence.service.ICatalogService;
import com.timbre.sec.persistence.service.ICategoryService;
import com.timbre.sec.persistence.service.IDescriptionService;
import com.timbre.sec.persistence.service.IGalleryService;
import com.timbre.sec.persistence.service.IProductReviewService;
import com.timbre.sec.persistence.service.IProductService;
import com.timbre.spring.persistence.setup.ApplicationDataBaseLoadApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.bean.RequestScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductReviewJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;
import org.slf4j.Logger;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.test.context.ContextConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.channels.ReadableByteChannel;


@ManagedBean(name="dynamicImageController", eager=true)
@SessionScoped

//@Configuration
public class DynamicImageController implements Serializable { //,   
//ApplicationListener<ContextRefreshedEvent> {
	
	
	public ArrayList<ProductJPA> getProductList() {
		return productList;
	}
	
	private StreamedContent logo1;
	
	private StreamedContent graphicText1;

	public void setProductList(ArrayList<ProductJPA> productList) {
		this.productList = productList;
	}

	private boolean setupDone = false;

	@Autowired
	private ApplicationContext context;

	// private JpaTestHelper testHelper = new JpaTestHelper();
	private static final Log log = LogFactory
			.getLog(ApplicationDataBaseLoadApi.class);

	@PersistenceContext
	private EntityManager entityManager;

	public ArrayList<ProductJPA> productList = null;

	public ApplicationContext getContext() {
		return context;
	}

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Autowired
	IProductService productService;

	public IProductService getProductService() {
		return productService;
	}

	public void setProductService(IProductService productService) {
		this.productService = productService;
	}

	@Autowired
	ICategoryService categoryService;

	public ICategoryService getCategroyService() {
		return categoryService;
	}

	public void setCategroyService(ICategoryService categoryService) {
		this.categroyService = categoryService;
	}

	@Autowired
	ICatalogService catalogService;

	public ICatalogService getCatalogService() {
		return catalogService;
	}

	public void setCatalogService(ICatalogService catalogService) {
		this.catalogService = catalogService;
	}

	@Autowired
	IDescriptionService descriptionService;

	public IDescriptionService getDescriptionService() {
		return descriptionService;
	}

	public void setDescriptionService(IDescriptionService descriptionService) {
		this.descriptionService = descriptionService;
	}

	@Autowired
	IProductReviewService productReviewService;

	public IProductReviewService getProductReviewServices() {
		return productReviewService;
	}

	public void setProductReviewServices(
			IProductReviewService productReviewService) {
		this.productReviewService = productReviewService;
	}

	@Autowired
	IGalleryService galleryService;

	public IGalleryService getGalleryService() {
		return galleryService;
	}

	public void setGalleryService(IGalleryService galleryService) {
		this.galleryService = galleryService;
	}

	XMLUtility xmlUtil = new XMLUtility();

	final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(ApplicationDataBaseLoadApi.class);

	private EntityFactoryJPA factory = new EntityFactoryJPA();
	
	private static final long serialVersionUID = 1L;
  
    private StreamedContent graphicText;  
      
    private StreamedContent barcode;  
      
    private StreamedContent chart;  
    
    private StreamedContent logo;
    
    private String testValue = "testValue";
  
    public String getTestValue() {
		return testValue;
	}

	public void setTestValue(String testValue) {
		this.testValue = testValue;
	}

	public StreamedContent getLogo1() {
		return logo1;
	}

	public void setLogo1(StreamedContent logo) {
		this.logo1 = logo;
	}

	public DynamicImageController() {  
        try {  
            //Graphic Text  
            BufferedImage bufferedImg = new BufferedImage(100, 25, BufferedImage.TYPE_INT_RGB);  
            Graphics2D g2 = bufferedImg.createGraphics();  
            g2.drawString("This is a text", 0, 10);  
            ByteArrayOutputStream os = new ByteArrayOutputStream();  
            ImageIO.write(bufferedImg, "png", os);  
            graphicText1 = new DefaultStreamedContent(new ByteArrayInputStream(os.toByteArray()), "image/png");   
  
            //Chart  
            JFreeChart jfreechart = ChartFactory.createPieChart("Turkish Cities", createDataset(), true, true, false);  
            File chartFile = new File("dynamichart");  
            ChartUtilities.saveChartAsPNG(chartFile, jfreechart, 375, 300);  
            chart = new DefaultStreamedContent(new FileInputStream(chartFile), "image/png");  
  
            //Barcode  
            File barcodeFile = new File("dynamicbarcode");  
            BarcodeImageHandler.saveJPEG(BarcodeFactory.createCode128("PRIMEFACES"), barcodeFile);  
            barcode = new DefaultStreamedContent(new FileInputStream(barcodeFile), "image/jpeg");  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }
	
	
	@Override
	public final void onApplicationEvent(final ContextRefreshedEvent  event) {
		if (!setupDone) {
			
			ApplicationContext applicationContext = event.getApplicationContext();
			

			DataBaseLoad();
			//dynamicImageLoad();
			
			setupDone = true;
		}
	}
	
	

	public void DataBaseLoad() {

		CatalogJPA catalog = new CatalogJPA();

		catalog.setCatalogCode("Fall2013");

		catalog.setCreationDate(new Date());

		// First we create it
		// catalogService.create(catalog);

		CategoryJPA category = new CategoryJPA();

		category.setCatalog(catalog);

		category.setCatalogId(catalog.getCatalogId());

		//
		catalog.addCategory(category);

		catalogService.create(catalog);// we should check that update(catalog);
										// works
		// Do we need to implement just save to keep in memory
		catalogService.saveAndFlush(catalog);

		categoryService.create(category);
		categoryService.saveAndFlush(category);

		
		 * //ProductJPA product = null; ProductJPA product = new ProductJPA();
		 * product.setCategory(category);
		 * product.setCategoryId(category.getCategoryId());
		 * 
		 * DescriptionJPA description = new DescriptionJPA(); //
		 * myExample.setMimeType("image/jpg");
		 * description.setProductCode("SweatHeartTank");
		 * description.setProductId(product.getProductId());
		 * 
		 * String prodDesc =
		 * "High-performance meets style in these versatile work-out bottoms.";
		 * 
		 * description.setProductId(product.getProductId());
		 * description.setProductJPA(product);
		 * 
		 * description.setProdDesc(prodDesc);
		 * 
		 * product.setDescription(description);
		 * 
		 * ProductReviewJPA review = new ProductReviewJPA(); String prodreview =
		 * "Damn Nice Product"; review.setReview(prodreview); Integer val = new
		 * Integer(3); review.setRating(val);
		 * review.setProductId(product.getProductId());
		 * review.setProductJPA(product);
		 * 
		 * product.addProductReview(review);
		 * 
		 * GalleryJPA gallery = new GalleryJPA(); String imageUrl =
		 * "resources/images"; gallery.setImageUrl(imageUrl);
		 * gallery.setProductId(product.getProductId());
		 * gallery.setProductJPA(product);
		 * 
		 * product.setGallery(gallery);
		 

		*//**** Begining of the XML Load ***********//*
		ProductJPA product = null;
		DescriptionJPA description = null;
		GalleryJPA gallery = null;
		File file = null;
		ProductReviewJPA review = null;
		try {
			File athleticBottomsDescription = new File(
					"AthleticBottomsDescriptions.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(athleticBottomsDescription);
			doc.getDocumentElement().normalize();

			// Read more:
			// http://javarevisited.blogspot.com/2011/12/parse-xml-file-in-java-example-tutorial.html#ixzz2NkGknhaW

			// normalize text representation
			doc.getDocumentElement().normalize();
			System.out.println("Root element of the doc is: "
					+ doc.getDocumentElement().getNodeName());

			NodeList listOfGarments = doc.getElementsByTagName("garment");
			int totalGarments = listOfGarments.getLength();
			System.out.println("Total number of Garments: " + totalGarments);

			for (int s = 0; s < listOfGarments.getLength(); s++) {

				product = new ProductJPA();
				product.setCategory(category);
				product.setCategoryId(category.getCategoryId());

				gallery = new GalleryJPA();
				//file = new File("resources/1finalLogoFitLabs.jpg");
				gallery.setMimeType("image/png");
				gallery.setProductId(product.getProductId());
				gallery.setProductJPA(product);
				// Attemp to Load image 
				try
				{
				  // Lets open an image file
				  gallery.setImage(readImageOldWay(file));
				}
				catch (IOException ex)
				{
				  //Logger.getLogger(Main.class.getName()).log(Level.ALL, null, ex);
				}
				
					
			
			
				

				description = new DescriptionJPA();
				description.setProductId(product.getProductId());
				description.setProductJPA(product);

				Node firstGarmentNode = listOfGarments.item(s);
				if (firstGarmentNode.getNodeType() == Node.ELEMENT_NODE) {

					Element firstGarmentElement = (Element) firstGarmentNode;

					// -------
					NodeList garmentDescriptionList = firstGarmentElement
							.getElementsByTagName("garment-description");
					Element firstDescriptionElement = (Element) garmentDescriptionList
							.item(0);

					NodeList textGarmentDescriptionList = firstDescriptionElement
							.getChildNodes();

					description.setProdDesc(((Node) textGarmentDescriptionList
							.item(0)).getNodeValue().trim());
					System.out.println("Garment DescriptionJPA: "
							+ ((Node) textGarmentDescriptionList.item(0))
									.getNodeValue().trim());

					// ---- urls

					NodeList imageUrlList = firstGarmentElement
							.getElementsByTagName("image-url");
					Element firstImageUrlElement = (Element) imageUrlList
							.item(0);

					NodeList textImageUrlList = firstImageUrlElement
							.getChildNodes();

					gallery.setImageUrl(((Node) textImageUrlList.item(0))
							.getNodeValue().trim());
					
					System.out.println("ImageURl : "
							+ ((Node) textImageUrlList.item(0)).getNodeValue()
									.trim());
					
					// - img file name
					
					NodeList imgList = firstGarmentElement
					.getElementsByTagName("img");
					Element firstImgElement = (Element) imgList
					.item(0);

					NodeList textImgList = firstImgElement
					.getChildNodes();

					gallery.setImg(((Node) textImgList.item(0))
					.getNodeValue().trim());
			
					System.out.println("Img fileName : "
					+ ((Node) textImgList.item(0)).getNodeValue()
							.trim());

					// --- Product-Code

					NodeList productCode = firstGarmentElement
							.getElementsByTagName("product-code");
					Element firstProductCodeElement = (Element) productCode
							.item(0);

					NodeList textProductCode = firstProductCodeElement
							.getChildNodes();

					product.setProductCode(((Node) textProductCode.item(0))
							.getNodeValue().trim());
					System.out.println("Product Code : "
							+ ((Node) textProductCode.item(0)).getNodeValue()
									.trim());

					// -- Style-Number

					NodeList styleNumber = firstGarmentElement
							.getElementsByTagName("style-number");
					Element firstStyleNumberElement = (Element) styleNumber
							.item(0);

					NodeList textStyleNumber = firstStyleNumberElement
							.getChildNodes();

					product.setStyleNumber(((Node) textStyleNumber.item(0))
							.getNodeValue().trim());
					System.out.println("Style Number : "
							+ ((Node) textStyleNumber.item(0)).getNodeValue()
									.trim());

					// -- Color originally NodeList colorList

					NodeList nodes = firstGarmentElement
							.getElementsByTagName("color");

					for (int x = 0; x < nodes.getLength(); x++) {
						Node node = nodes.item(x);
						if (node.getNodeName().equalsIgnoreCase("color")) {
							NodeList childNodes = node.getChildNodes();
							for (int y = 0; y < childNodes.getLength(); y++) {
								Node data = childNodes.item(y);
								if (data.getNodeType() == Node.TEXT_NODE)
									System.out.println("Color : "
											+ ((Node) childNodes.item(y))
													.getNodeValue().trim());

								String col = ((Node) childNodes.item(y))
										.getNodeValue().trim();
								String col1 = new String(col);
								// product.setColors(((Node)textColor.item(x)).getNodeValue().trim());
								product.addNewColorItem(col1);
								// return data.getNodeValue();
							}
						}
					}

					NodeList sizenodes = firstGarmentElement
							.getElementsByTagName("size");

					for (int x = 0; x < sizenodes.getLength(); x++) {
						Node node = sizenodes.item(x);
						if (node.getNodeName().equalsIgnoreCase("size")) {
							NodeList childNodes = node.getChildNodes();
							for (int y = 0; y < childNodes.getLength(); y++) {
								Node data = childNodes.item(y);
								if (data.getNodeType() == Node.TEXT_NODE)
									System.out.println("Size : "
											+ ((Node) childNodes.item(y))
													.getNodeValue().trim());

								String siz = ((Node) childNodes.item(y))
										.getNodeValue().trim();
								String siz1 = new String(siz);
								// product.setColors(((Node)textColor.item(x)).getNodeValue().trim());
								product.addNewSize(siz1);
								// return data.getNodeValue();
							}
						}
					}

				}// end of if clause

				// Set the Product on description since we have got all the
				// values
				product.setDescription(description);
				// Set the Product on gallery since we have got all the values
				product.setGallery(gallery);

			

		// - Add Reviews Since values dont come from xml
		review = new ProductReviewJPA();
		String prodreview = "Damn Nice Product";
		review.setReview(prodreview);
		Integer val = new Integer(3);
		review.setRating(val);
		review.setProductId(product.getProductId());
		review.setProductJPA(product);

		product.addProductReview(review);

		// First we persist the Product so the DB will not complian about not
		// knowing productId
		productService.create(product);
		productService.saveAndFlush(product);

		descriptionService.create(description);
		descriptionService.saveAndFlush(description);

		productReviewService.create(review);
		productReviewService.saveAndFlush(review);

		category.addProduct(product);

		category.setCreationDate(new Date());
		category.setCategoryName("Athletic");

		categoryService.update(category);
		categoryService.saveAndFlush(category);

		catalogService.update(catalog);// we should check that update(catalog);
										// works
		catalogService.saveAndFlush(catalog);
		
		}// end of for loop with s var

		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}

		CategoryJPA caty = categoryService.findByCategoryId(category
				.getCategoryId());
		CatalogJPA cat = catalogService.findByCatalogId(catalog.getCatalogId());// findByName("TimsTest1");

		@SuppressWarnings("unused")
		String catName = cat.getCatalogCode();

		Set<ICategory> cats = new HashSet<ICategory>();

		Set<IProduct> prods = new HashSet<IProduct>();

		productList = new ArrayList<ProductJPA>();

		cats.addAll((Collection<? extends ICategory>) cat.getCategories());

		for (Iterator<ICategory> it = cats.iterator(); it.hasNext();) {

			Object o = it.next();

			CategoryJPA cat1 = (CategoryJPA) o;

			prods.addAll((Collection<? extends IProduct>) cat1.getProducts());

			for (Iterator<IProduct> itOverProds = prods.iterator(); itOverProds
					.hasNext();) {
				Object obj = itOverProds.next();
				ProductJPA e = (ProductJPA) obj;
				productList.add(e);

			}

			for (Iterator<ProductJPA> itOverPList = productList.iterator(); itOverPList.hasNext();) {
				ProductJPA p = itOverPList.next();
				System.out.println("Product values CategoryId :"+p.getCategoryId());
				System.out.println("Product values ProductCode :"+p.getProductCode());
				System.out.println("Product values ProductId :"+p.getProductId());
				
				Collection<String> c = p.getColors();
				
				for(Iterator<String> itOverC = c.iterator(); itOverC.hasNext();){
						
						Object oString = itOverC.next();
						
						String colorVal = (String) oString;
					
						System.out.println("Color values :"+colorVal);
					}
				
				System.out.println("Product values Description :"+p.getDescription().getProdDesc());
				System.out.println("Product values ImageUrl :"+p.getGallery().getImageUrl());
				System.out.println("Product values Img FileName :"+p.getGallery().getImg());
				Collection<String> siz = p.getSizes();
				for (Iterator<String> itOverSiz = siz.iterator(); itOverSiz.hasNext();){
					
					System.out.println("Sizes values :"+itOverSiz.next());
			}
				

			}
			
			

		}
	}
	
	
	
	public byte[] readImageOldWay(File file) throws IOException
	{
	  //Logger.getLogger(Main.class.getName()).log(Level.INFO, "[Open File] " + file.getAbsolutePath());
	  InputStream is = new FileInputStream(file);
	  // Get the size of the file
	  long length = file.length();
	  // You cannot create an array using a long type.
	  // It needs to be an int type.
	  // Before converting to an int type, check
	  // to ensure that file is not larger than Integer.MAX_VALUE.
	  if (length > Integer.MAX_VALUE)
	  {
	    // File is too large
	  }
	  // Create the byte array to hold the data
	  byte[] bytes = new byte[(int) length];
	  // Read in the bytes
	  int offset = 0;
	  int numRead = 0;
	  while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)
	  {
	    offset += numRead;
	  }
	  // Ensure all the bytes have been read in
	  if (offset < bytes.length)
	  {
	    throw new IOException("Could not completely read file " + file.getName());
	  }
	  // Close the input stream and return bytes
	  is.close();
	  return bytes;
	}
	
	
	
	
	private void createURLReferences() {
        String classpath = System.getProperty("java.class.path");
        String[] classpathEntries = classpath.split(System.getProperty("path.separator"));
        //List<URL> urls = new ArrayList<URL>();
        for (String classpathEntry : classpathEntries) {
            File classpathFile = new File(classpathEntry);
            URI uri = classpathFile.toURI();
            try {
                URL url = uri.toURL();
                //urls.add(url);
                System.out.println("Classpath entry: " + classpathEntry);
            } catch (MalformedURLException e) {
                System.out.println("Ignoring classpath entry: " + classpathEntry);
            }
        }

        //return urls.toArray(new URL[urls.size()]);
    }

	
	
	
	public void setLogo(StreamedContent logo) {
		this.logo1 = logo;
	}

	public StreamedContent getLogo() {
		return logo1;
	}

	public void setGraphicText(StreamedContent graphicText) {
		this.graphicText1 = graphicText;
	}

	public StreamedContent getGraphicText1() {
		return graphicText1;
	}
    
    public String dynamicImageLoad() {  
        try {  
            System.out.println("We are in dynamicImageLoad");  
          //Streaming File
            // Opens a resource from the current class' defining class loader
               //InputStream istream = getClass().getResourceAsStream("1finalLogoFitLabs.jpg");

               // Create a NIO ReadableByteChannel from the stream
               //ReadableByteChannel channel = java.nio.channels.Channels.newChannel(istream);
               URL url = new URL("file:///path/to/customClasses.jar");
               File logoFile = new File("1finalLogoFitLabs.jpg");  
                 
               logo1 = new DefaultStreamedContent(new FileInputStream(logoFile), "image/jpeg");    
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }
      
    public StreamedContent getBarcode() {  
        return barcode;  
    }  
  
    public StreamedContent getGraphicText() {  
        return graphicText1;  
    }  
          
    public StreamedContent getChart() {  
        return chart;  
    }  
      
    private PieDataset createDataset() {  
        DefaultPieDataset dataset = new DefaultPieDataset();  
        dataset.setValue("Istanbul", new Double(45.0));  
        dataset.setValue("Ankara", new Double(15.0));  
        dataset.setValue("Izmir", new Double(25.2));  
        dataset.setValue("Antalya", new Double(14.8));  
  
        return dataset;  
    }  
}  
*/