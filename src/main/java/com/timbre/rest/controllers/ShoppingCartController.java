package com.timbre.rest.controllers;

import java.io.IOException;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.timbre.sec.model.products.ProductJPA;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ShoppingCartController {

    /*@RequestMapping(value = "/users/cart", method = RequestMethod.POST, consumes = { "application/json" })
    public void submitCart(@RequestParam("Product")  Map<String, String> product) {
           
    }*/
    //method = RequestMethod.POST, produces="application/json", consumes="application/json"
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/users/cart", consumes = "application/json", produces = "application/json")
	public void process(@RequestBody ProductJPA product) 
		    throws Exception {

		System.out.println("************* Passed in POST Product Name **********" + product.getSize());

        if (product != null) {
            // persist the CartObject
        	System.out.println(product);
        }

		}
	
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/users/cart1/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProductJPA> createCartObject(@RequestBody ProductJPA product) {
    	
    	System.out.println("************* Passed in POST Product Name **********" + product);

        if (product != null) {
            // persist the CartObject
        	System.out.println(product);
        }

        // TODO: call persistence layer to update
        return new ResponseEntity<ProductJPA>(product, HttpStatus.OK);
    }
	
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/users/cart2/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProductJPA> createCartObject2(@RequestBody String product) {
    	
    	System.out.println("************* Passed in POST Product Name **********" + product.toString());
    	
    	ObjectMapper mapper = new ObjectMapper();
    	
    	
    	ProductJPA obj = null;
		try {
			obj = mapper.readValue(product, ProductJPA.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        if (product != null) {
            // persist the CartObject
        	System.out.println(obj);
        }

        // TODO: call persistence layer to update
        return new ResponseEntity<ProductJPA>(obj, HttpStatus.OK);
    }
	
	/*@CrossOrigin(origins = "*")
	@PostMapping(path = "/users/cart2", consumes = "application/json", produces = "application/json")
    public ResponseEntity<JSONObject> createCartObject2(@RequestBody JSONObject product) {
    	
    	System.out.println("************* Passed in POST Product Name **********" + product.toString());

        if (product != null) {
            // persist the CartObject
        	System.out.println(product);
        }

        // TODO: call persistence layer to update
        return new ResponseEntity<JSONObject>(product, HttpStatus.OK);
    }*/
	
	
    
    
    /*@RequestMapping(value = "/users/cart/{id}", method = RequestMethod.POST, consumes = { "application/json" })
   // @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void updatePackage(@PathVariable String id, @RequestBody JSONObject product) {
        if (product == null) {
           // throw new BadRequestException("Given product is null");
        }

       // return softwarePackageService.updatePackage(id,sPackage);
    }*/
}