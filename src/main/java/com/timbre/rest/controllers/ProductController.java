package com.timbre.rest.controllers;

import org.json.JSONException;
//import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.timbre.rest.services.RestProductJsonService;
import com.timbre.sec.model.products.ProductJPA;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProductController {
	
	private RestProductJsonService restProductJsonService; 
	
	@Autowired
	public ProductController(RestProductJsonService restProductJsonService){
		
		Assert.notNull(restProductJsonService, "restCatalogJsonService must not be null!");
        this.restProductJsonService = restProductJsonService;
		
	}
		
	// mvn spring-boot:run
	//mvn package && java -jar target/fitlabsSpringCloud-0.0.1-SNAPSHOT.jar
		//curl -X POST localhost:8080/shutdown
	//curl localhost:8080/catalog
	
	
	@CrossOrigin(origins = "*")
    @GetMapping("/productId")
    public ProductJPA product(@RequestParam("id") String id) throws JSONException {
        
        return restProductJsonService.getProductById(id);
    }
	
	@CrossOrigin(origins = "*")
    @GetMapping("/productName")
    public ProductJPA productByName(@RequestParam("ProductName") String productName) throws JSONException {
		System.out.println("************* Product Name **********" + productName);
        return restProductJsonService.getProductByName(productName);
    }
	
	

}


