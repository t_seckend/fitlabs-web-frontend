package com.timbre.rest.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VideoController {

	/*@RequestMapping(value = "/video/{id}", method = RequestMethod.GET)
	public ResponseEntity<ClassPathResource> getFullVideo(@PathVariable("id") String id) throws IOException {
		ClassPathResource videoFile = new ClassPathResource("resources/video/"+id+".mp4");
		
		return ResponseEntity.status(HttpStatus. PARTIAL_CONTENT)
				.contentType(MediaTypeFactory
						.getMediaType(videoFile)
						.orElse(MediaType.APPLICATION_OCTET_STREAM))
				.body(videoFile);
	}*/
    
    
	@RequestMapping(value = "/video/{id}", method = RequestMethod.GET, produces = "video/mp4")
	@ResponseBody public ClassPathResource getPreview3(@PathVariable("id") String id, HttpServletResponse response) {
		ClassPathResource videoFile = new ClassPathResource("resources/video/"+id+".mp4");
	    return videoFile;
	}
    
    
    /*@RequestMapping(value = "/video/{id}", method = RequestMethod.GET, produces = "video/mp4")
    public ResponseEntity<ClassPathResource> getVideo(@PathVariable("id") String id) throws IOException {
        //byte[] image = imageService.getImage(id);
    	ClassPathResource videoFile = new ClassPathResource("resources/video/"+id+".mp4");
        
        
        
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
    			.contentType(MediaTypeFactory
    					.getMediaType(videoFile)
    					.orElse(MediaType.APPLICATION_OCTET_STREAM))
    			.body(videoFile);
    }*/
    
    /*@RequestMapping(value = "/sid", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage() throws IOException {

        ClassPathResource imgFile = new ClassPathResource("resources/12100110F-1X.RIGHT.jpg");
        byte[] bytes = StreamUtils.copyToByteArray(imgFile.getInputStream());

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
    }*/
}

