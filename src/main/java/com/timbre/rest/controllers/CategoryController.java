package com.timbre.rest.controllers;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.*;

import org.json.JSONArray;
import org.json.JSONException;
//import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.timbre.rest.services.RestCatalogJsonService;
import com.timbre.rest.services.RestProductJsonService;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class CategoryController {
	
	private RestCatalogJsonService restCatalogJsonService; 
	private RestProductJsonService restProductJsonService; 
	
	@Autowired
	public CategoryController(RestCatalogJsonService restCatalogJsonService, RestProductJsonService restProductJsonService){
		
		Assert.notNull(restCatalogJsonService, "restCatalogJsonService must not be null!");
        this.restCatalogJsonService = restCatalogJsonService;
        this.restProductJsonService = restProductJsonService;
		
	}
		
	
	
	@CrossOrigin(origins = "*")
    @GetMapping("/category")
    public Collection<IProduct> categoryByName(@RequestParam("CategoryName") String categoryName) throws JSONException {
		System.out.println("************* Category Name CategoryController **********" + categoryName);
        return restCatalogJsonService.getCategoryByName(categoryName);
    }

}


