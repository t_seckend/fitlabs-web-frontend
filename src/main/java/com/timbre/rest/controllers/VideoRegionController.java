package com.timbre.rest.controllers;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.lang.Long;
import java.net.MalformedURLException;

@RestController
class VideoRegionController {
	
	Long ChunkSize = 1000000L;

    @GetMapping("/videos/{name}/full")
    ResponseEntity<UrlResource> getFullVideo(@PathVariable("name") String name) throws MalformedURLException {
    	 UrlResource video =  new UrlResource("resources/video/"+name+".mp4");
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .contentType(MediaTypeFactory.getMediaType(video).orElse(MediaType.APPLICATION_OCTET_STREAM))
                .body(video);
    }

    @GetMapping("/videos/{name}")
    ResponseEntity<ResourceRegion> getVideo(@PathVariable("name") String name, @RequestHeader HttpHeaders headers ) throws IOException {
    	UrlResource video = new UrlResource("resources/video/"+name+".mp4");
    			ResourceRegion region = resourceRegion(video, headers);
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .contentType(MediaTypeFactory.getMediaType(video).orElse(MediaType.APPLICATION_OCTET_STREAM))
                .body(region);
    }

    private  ResourceRegion resourceRegion(UrlResource video, HttpHeaders headers) throws IOException {
        Long contentLength = video.contentLength();
        //Long range = headers.RANGE.  firstOrNull();
        /*return if (range != null) {
            val start = range.getRangeStart(contentLength);
            val end = range.getRangeEnd(contentLength);
            val rangeLength = min(ChunkSize, end - start + 1);
            ResourceRegion(video, start, rangeLength);
        } else {
            val rangeLength = min(ChunkSize, contentLength);
            ResourceRegion(video, 0, rangeLength);
        }*/
        
        return null;
    }

    

}
