package com.timbre.rest.services;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.timbre.sec.model.products.ProductJPA;

import com.timbre.sec.persistence.service.IProductService;

@Service
public class RestProductJsonService {
	
	
	
		IProductService productService;
		
		@Autowired
		public RestProductJsonService(IProductService productService){
			this.productService = productService;
		}
		
		public ProductJPA getProductByName(String productName) {
			
			return productService.findByName(productName);
		}
		
		public ProductJPA getProductById(String productId) {
			
			return productService.findByProductId(productId);
			
		}
		
	    

	

}
