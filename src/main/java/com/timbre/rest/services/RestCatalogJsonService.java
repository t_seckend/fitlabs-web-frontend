package com.timbre.rest.services;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

//import org.json.JSONArray;
//import org.json.JSONException;
import com.google.gson.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.persistence.service.ICategoryService;

@Service
public class RestCatalogJsonService {
	
	
	
		ICategoryService categoryService;
		public static String categoryName = "AthleticTopsDescNov19.xml";
		public ArrayList<ProductJPA> garmentList;
		JSONArray array = new JSONArray();
		@Autowired
		public RestCatalogJsonService(ICategoryService categoryService){
			this.categoryService = categoryService;
		}
		
	    

	public Collection<IProduct> getGarmentList() throws JSONException {

		CategoryJPA oneCat = categoryService.findByCategoryName(categoryName);
		
		
		@SuppressWarnings("unused")

		Set<ICategory> cats = new HashSet<ICategory>();

		Set<IProduct> prods = new HashSet<IProduct>();

		System.out.println("************* Category Name **********" + oneCat.getCategoryName());
		if (oneCat.getCategoryName().equalsIgnoreCase("AthleticTopsDescNov19.xml"))

			prods.addAll((Collection<? extends IProduct>) oneCat.getProducts());

		return prods;
	}
	
	public Collection<IProduct> getCategoryByName(String catName) throws JSONException {

		CategoryJPA oneCat = categoryService.findByCategoryName(catName);
		if(oneCat == null) {
			System.out.println("*************  The Category is Null In Catalog Service**********");
			
		}

		@SuppressWarnings("unused")

		Set<ICategory> cats = new HashSet<ICategory>();

		Set<IProduct> prods = new HashSet<IProduct>();

		System.out.println("************* Category Name In Catalog Service**********" + oneCat.getCategoryName());
		if (oneCat.getCategoryName().equalsIgnoreCase(catName))

			prods.addAll((Collection<? extends IProduct>) oneCat.getProducts());

		return prods;
	}

}
