package com.timbre.sec.persistence.service;

import com.timbre.persistence.service.IService;
import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;


public interface IDescriptionService extends IService<DescriptionJPA> {
	
	DescriptionJPA findByProductCode(String code);
	
	DescriptionJPA findByProductId(String productid);
	
	void saveAndFlush(DescriptionJPA description);

}
