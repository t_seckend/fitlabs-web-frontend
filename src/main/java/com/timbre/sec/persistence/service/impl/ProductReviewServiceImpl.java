package com.timbre.sec.persistence.service.impl;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.products.IProductReview;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.model.products.ProductReviewJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.persistence.dao.IDescriptionJpaDAO;
import com.timbre.sec.persistence.dao.IProductReviewJpaDAO;
import com.timbre.sec.persistence.service.IDescriptionService;
import com.timbre.sec.persistence.service.IProductReviewService;




@Transactional
@Repository("ProductReviewRepository")
public class ProductReviewServiceImpl extends AbstractService< ProductReviewJPA > implements IProductReviewService{
	
	
	@Autowired
	IProductReviewJpaDAO dao;
	
	

	public ProductReviewServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	/*public GalleryJPA getPersonByLogin(String login) {
		return (Principal) em.createQuery("select p from USER p where p.login = :login").setParameter("login", login).getSingleResult();		
	}*/



	// API

	@Override
	public ProductReviewJPA findByProductId( final String productid ){
		return dao.findByProductId( productid );
	}
	
	@Override
	public ProductReviewJPA findByProductCode( final String productcode ){
		return dao.findByProductCode( productcode );
	}

	// Spring

	@Override
	protected final IProductReviewJpaDAO getDao(){
		return dao;
	}


	public void saveAndFlush(ProductReviewJPA product) {
		dao.saveAndFlush(product);
		
	}

}
