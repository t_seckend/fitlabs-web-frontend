package com.timbre.sec.persistence.service;

import com.timbre.persistence.service.IService;
import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.products.ProductJPA;


public interface ICatalogService extends IService<CatalogJPA> {
	
	CatalogJPA findByCatalogCode( final String name );

	CatalogJPA findByCatalogId(String catalogid);
	
	void saveAndFlush(CatalogJPA catalog);
}
