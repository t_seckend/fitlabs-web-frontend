package com.timbre.sec.persistence.service;

import com.timbre.persistence.service.IService;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;

public interface IGalleryService extends IService< GalleryJPA >{
	
	GalleryJPA findByProductCode( final String code );
	
	GalleryJPA findByProductId( final String productid);
	
	void saveAndFlush(GalleryJPA gallery);

}
