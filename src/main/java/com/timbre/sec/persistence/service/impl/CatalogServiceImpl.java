package com.timbre.sec.persistence.service.impl;


//import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.products.ProductJPA;


import com.timbre.sec.persistence.dao.ICatalogJpaDAO;

import com.timbre.sec.persistence.service.ICatalogService;


//@ManagedBean(name = "catalogService")


@Transactional
@Repository("CatalogRepository")

public class CatalogServiceImpl extends AbstractService< CatalogJPA > implements ICatalogService{
	
	@Autowired
	ICatalogJpaDAO dao;
	
	

	public CatalogServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	// API

	@Override
	public CatalogJPA findByCatalogCode( final String name ){
		return dao.findByCatalogCode( name );
	}
	
	@Override
	public CatalogJPA findByCatalogId(String catalogid){
		return dao.findByCatalogId( catalogid );
	}
	
	public void saveAndFlush(CatalogJPA catalog){
		dao.saveAndFlush(catalog);
	}

	// Spring

	@Override
	protected final ICatalogJpaDAO getDao(){
		return dao;
	}

}
