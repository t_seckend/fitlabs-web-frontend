package com.timbre.sec.persistence.service;

import java.util.List;

import com.timbre.persistence.service.IService;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.products.Comment;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.model.products.ProductReviewJPA;


public interface IProductReviewService extends IService<ProductReviewJPA> {
	
	ProductReviewJPA findByProductCode( final String code );
	
	ProductReviewJPA findByProductId(String productid);

	void saveAndFlush(ProductReviewJPA productreview);
	

	
}
