package com.timbre.sec.persistence.service.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.category.CategoryJPA;


import com.timbre.sec.persistence.dao.ICatalogJpaDAO;
import com.timbre.sec.persistence.dao.ICategoryJpaDAO;

import com.timbre.sec.persistence.service.ICatalogService;
import com.timbre.sec.persistence.service.ICategoryService;


//@ManagedBean(name = "categoryService")


@Transactional
@Repository("CategoryRepository")

public class CategoryServiceImpl extends AbstractService< CategoryJPA > implements ICategoryService{
	
	@Autowired
	ICategoryJpaDAO dao;
	
	

	public CategoryServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	/*public GalleryJPA getPersonByLogin(String login) {
		return (Principal) em.createQuery("select p from USER p where p.login = :login").setParameter("login", login).getSingleResult();		
	}*/



	// API

	@Override
	public CategoryJPA findByCategoryName( final String name ){
		return dao.findByCategoryName( name );
	}
	
	@Override
	public CategoryJPA findByCategoryId(String categoryid){
		return dao.findByCategoryId( categoryid );
	}

	// Spring

	@Override
	protected final ICategoryJpaDAO getDao(){
		return dao;
	}


	@Override
	public void saveAndFlush(CategoryJPA category) {
		dao.saveAndFlush(category);
		
	}

}
