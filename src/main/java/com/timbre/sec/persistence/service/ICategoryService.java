package com.timbre.sec.persistence.service;

import com.timbre.persistence.service.IService;
import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.category.CategoryJPA;
import com.timbre.sec.model.products.ProductJPA;

public interface ICategoryService extends IService<CategoryJPA> {
	
	CategoryJPA findByCategoryName( final String name );

	CategoryJPA findByCategoryId(String categoryid);
	
	void saveAndFlush(CategoryJPA category);
}
