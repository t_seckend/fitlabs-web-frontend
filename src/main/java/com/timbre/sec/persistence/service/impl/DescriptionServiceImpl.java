package com.timbre.sec.persistence.service.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.persistence.dao.IDescriptionJpaDAO;
import com.timbre.sec.persistence.service.IDescriptionService;




@Transactional
@Repository("DescriptionRepository")
public class DescriptionServiceImpl extends AbstractService< DescriptionJPA > implements IDescriptionService{
	
	
	@Autowired
	IDescriptionJpaDAO dao;
	
	

	public DescriptionServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	
	// API

	@Override
	public DescriptionJPA findByProductId(String productid ){
		return dao.findByProductId( productid );
	}
	
	@Override
	public DescriptionJPA findByProductCode(String productname ){
		return dao.findByProductCode( productname );
	}

	// Spring

	@Override
	protected final IDescriptionJpaDAO getDao(){
		return dao;
	}



	@Override
	public void saveAndFlush(DescriptionJPA description) {
		dao.saveAndFlush(description);
		
	}

}
