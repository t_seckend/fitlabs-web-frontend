package com.timbre.sec.persistence.service.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.web.content.GalleryJPA;
import com.timbre.sec.persistence.dao.IGalleryJpaDAO;

import com.timbre.sec.persistence.service.IGalleryService;




@Transactional
@Repository("GalleryRepository")

public class GalleryServiceImpl extends AbstractService< GalleryJPA > implements IGalleryService{
	
	@Autowired
	IGalleryJpaDAO dao;
	
	

	public GalleryServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	public GalleryJPA findByProductId(String productid) {
		return dao.findByProductId( productid );		
	}



	// API

	@Override
	public GalleryJPA findByProductCode( final String name ){
		return dao.findByProductCode( name );
	}

	// Spring

	@Override
	protected final IGalleryJpaDAO getDao(){
		return dao;
	}


	@Override
	public void saveAndFlush(GalleryJPA gallery) {
		dao.saveAndFlush(gallery);
		
	}
	
		
	

}
