package com.timbre.sec.persistence.service.impl;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.service.AbstractService;

import com.timbre.sec.model.products.Comment;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;
//import com.timbre.sec.model.products.Product;
//import com.timbre.sec.model.shopping.impl.order.Order;
import com.timbre.sec.model.web.content.GalleryJPA;
//import com.timbre.sec.persistence.dao.ICustomerJpaDAO;
import com.timbre.sec.persistence.dao.ICatalogJpaDAO;
import com.timbre.sec.persistence.dao.IProductJpaDAO;
import com.timbre.sec.persistence.service.IProductService;

@Transactional
@Repository("ProductRepository")
public class ProductServiceImpl extends AbstractService< ProductJPA >implements IProductService {

	
	@Autowired
	IProductJpaDAO dao;
	
	public ProductServiceImpl(){
		super();
	}
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	@Override
	public ProductJPA findByProductCode(String name) {
		
		return dao.findByProductCode(name);
	}

	
	@Override
	public ProductJPA findByProductId(String productid) {
		
		return dao.findByProductId(productid);
	}
	
	public ProductJPA findByName(String productName) {
		return dao.findByName(productName);
	}
	
	@Override
	protected final IProductJpaDAO getDao(){
		return dao;
	}
	
	//@Override
	public void saveAndFlush(ProductJPA prod){
		dao.saveAndFlush(prod);
	}
	
	

	
	

}
