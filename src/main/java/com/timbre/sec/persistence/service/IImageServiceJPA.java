package com.timbre.sec.persistence.service;

import com.timbre.persistence.jpa.model.image.ImageJPA;
import com.timbre.persistence.service.IService;


public interface IImageServiceJPA extends IService< ImageJPA >{
	
	ImageJPA findByProductCode( final String code );
	
	ImageJPA findByProductId( final String imageid);
	
	ImageJPA findById( final String imageid);
	
	void saveAndFlush(ImageJPA image);

}