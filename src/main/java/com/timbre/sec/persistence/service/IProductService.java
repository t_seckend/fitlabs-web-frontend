package com.timbre.sec.persistence.service;

import java.util.List;

import com.timbre.persistence.service.IService;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.products.Comment;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;

public interface IProductService extends IService<ProductJPA> {
	
	ProductJPA findByProductCode( final String code );
	
	ProductJPA findByProductId(String productid);
	
	ProductJPA findByName(String productName);

	void saveAndFlush(ProductJPA product);

	
}
