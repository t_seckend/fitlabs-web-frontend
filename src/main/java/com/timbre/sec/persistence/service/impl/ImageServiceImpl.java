package com.timbre.sec.persistence.service.impl;




import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.timbre.persistence.jpa.model.image.ImageJPA;
import com.timbre.persistence.service.AbstractService;
import com.timbre.sec.persistence.dao.IImageJpaDAO;

import com.timbre.sec.persistence.service.IImageServiceJPA;




@Transactional
@Repository("ImageRepository")

public class ImageServiceImpl extends AbstractService< ImageJPA > implements IImageServiceJPA{
	
	@Autowired
	IImageJpaDAO dao;
	
	

	public ImageServiceImpl(){
		super();
	}
	
	
private EntityManager em = null;
    
	/**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

	
	public ImageJPA findByProductId(String imageid) {
		return dao.findByProductId( imageid );		
	}



	// API

	@Override
	public ImageJPA findByProductCode( final String name ){
		return dao.findByProductCode( name );
	}

	// Spring

	@Override
	protected final IImageJpaDAO getDao(){
		return dao;
	}


	@Override
	public void saveAndFlush(ImageJPA image) {
		dao.saveAndFlush(image);
		
	}


	@Override
	public ImageJPA findById(String imageid) {
		return dao.findById(imageid);
		
	}
	
		
	

}

