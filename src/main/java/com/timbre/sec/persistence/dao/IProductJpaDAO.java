package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.timbre.sec.model.products.ProductJPA;

public interface IProductJpaDAO extends JpaRepository< ProductJPA, Long >{

	ProductJPA findByProductCode(String name);

	ProductJPA findByProductId(String productid);
	
	ProductJPA findByName(String productName);

}
