package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.timbre.sec.model.web.content.GalleryJPA;

public interface IGalleryJpaDAO extends JpaRepository< GalleryJPA, Long >{
	
	
	
	GalleryJPA findByProductCode( final String name );
	
	GalleryJPA findByProductId( final String productid );
	
}
