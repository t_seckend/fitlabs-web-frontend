package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.timbre.persistence.jpa.model.image.ImageJPA;

public interface IImageJpaDAO extends JpaRepository< ImageJPA, Long >{
	
	
	
	ImageJPA findByProductCode( final String code );
	
	ImageJPA findByProductId( final String imageid );
	
	ImageJPA findById( final String imageid);
	
	ImageJPA saveAndFlush(ImageJPA image);
	
}
