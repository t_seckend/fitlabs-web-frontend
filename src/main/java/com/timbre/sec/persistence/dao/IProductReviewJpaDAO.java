package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.timbre.sec.model.products.ProductReviewJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;

//@Component
//@Service
public interface IProductReviewJpaDAO extends JpaRepository< ProductReviewJPA, Long >{

	ProductReviewJPA findByProductCode( final String name );
	
	ProductReviewJPA findByProductId( final String productid );
}

