package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.timbre.sec.model.catalog.CatalogJPA;


public interface ICatalogJpaDAO extends JpaRepository< CatalogJPA, Long >{

	CatalogJPA findByCatalogCode(String name);

	CatalogJPA findByCatalogId(String catalogid);

}
