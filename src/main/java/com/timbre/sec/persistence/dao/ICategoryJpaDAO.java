package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.category.CategoryJPA;

public interface ICategoryJpaDAO extends JpaRepository< CategoryJPA, Long >{

	CategoryJPA findByCategoryName(String name);

	CategoryJPA findByCategoryId(String categoryid);

}
