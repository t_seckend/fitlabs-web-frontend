package com.timbre.sec.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.timbre.sec.model.web.content.DescriptionJPA;


public interface IDescriptionJpaDAO extends JpaRepository< DescriptionJPA, Long >{

	DescriptionJPA findByProductCode(String name );
	
	DescriptionJPA findByProductId(String productid );
}


