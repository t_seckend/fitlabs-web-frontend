package com.timbre.sec.persistence.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.timbre.persistence.jpa.model.UserJPA;

@Component
//@Service

public interface IUserJpaDAO extends JpaRepository< UserJPA, Long >{
	
	
	
	UserJPA findByName( final String name );
	
	UserJPA findByEmail(final String email);
	
}
