package com.timbre.sec.model.catalog;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.timbre.common.IEntity;
import com.timbre.sec.model.catalog.api.ICatalog;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;




@Entity
//@DiscriminatorValue("collection")
@Table(name = "catalogjpa")
public class CatalogJPA  implements IEntity, ICatalog{

	private static final long serialVersionUID = 2873258323314048223L;

	//@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Id
	@Column( name = "catalogId", unique = true, nullable = false )
	private String catalogId;

	
	@Column(name = "createdate")
	//@Type(type="long_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "modifydate")
	//@Type(type="long_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;


	protected UUID idOne;
	public CatalogJPA()
	{
		this.setCatalogId(generateUid());
	}
	public String generateUid() {
		idOne = UUID.randomUUID();
		return String.valueOf(idOne);
	}

		

	
	@OneToMany(targetEntity=CategoryJPA.class, mappedBy = "catalogId",  cascade=CascadeType.ALL , fetch=FetchType.EAGER)
	
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Set<ICategory> iCategories = new HashSet<ICategory>(0);

	private String catalogCode;
	

	public String getCatalogCode() {
		return catalogCode;
	}
	public void setCatalogCode(String name) {
		this.catalogCode = name;
	}
	//	 Add a new item and update the total
	public void addCategory(ICategory newItem)
	{
		//See if there's already an item like this in the cart
		boolean currIndex = iCategories.isEmpty();	
		if (currIndex) {
			//If the item is new, add it to the cart
			iCategories.add(newItem);
		} else {
			Boolean itemExists=false;
			for (Iterator<ICategory> it = iCategories.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				CategoryJPA cat =(CategoryJPA)o;
				if(cat.getCategoryId() == newItem.getCategoryId()) {
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				iCategories.add(newItem);


		}
	}

	/*public void addItem(CartItem newItem)
{
	//See if there's already an item like this in the cart
	boolean currIndex = iCategories.isEmpty();	
	if (currIndex) {
		//If the item is new, add it to the cart
		iCategories.add(newItem);
	} else {
		Boolean itemExists=false;
		for (Iterator<CartItem> it = iCategories.iterator (); it.hasNext (); ) {
		    Object o = it.next ();
		    CartItem shop=(CartItem)o;
		    if(shop.getItemCode() == newItem.getItemCode()){
		    	//update
		    	shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
		    	itemExists=true;
		    }
		  }
		if(!itemExists)
			iCategories.add(newItem);


	}
}*/


	/*public void removeItem(Long long1) {
		//Find item
		for(int i=0; i<iCategories.size();i++ ) {
			Object o = ((List<?>) iCategories).get(i);
			ShoppingCartItem shop=(ShoppingCartItem)o;
			if(shop.getItemCode()==long1){	
				//Remove item
				iCategories.remove(i);
				break;
			}  
		}

	}*/

	public Set<ICategory> getCategories() {	
		return (Set<ICategory>) iCategories;	
	}



	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( catalogCode == null ) ? 0 : catalogCode.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final CatalogJPA other = (CatalogJPA) obj;
		if( catalogCode == null ){
			if( other.catalogCode != null )
				return false;
		}
		else if( !catalogCode.equals( other.catalogCode ) )
			return false;
		return true;
	}

	@Override
	public String toString(){
		return new ToStringBuilder( this ).append( "id", id ).append( "catalogCode", catalogCode ).toString();
	}


	public void empty() {
		iCategories.clear();

	}


	/*@Override
public String getEntityTag() {
	// TODO Auto-generated method stub
	return null;
}*/


	


	@Override
public void setId(Long id) {
	// TODO Auto-generated method stub

}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	
	public void updateTimestamp() {
		modifiedDate = new Date();
	}


	
	public void setCatalogId(String catalogid) {
		this.catalogId = catalogid;
	}
	public String getCatalogId() {
		return catalogId;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
