package com.timbre.sec.model.catalog.api;

import java.util.Date;

import com.timbre.common.IEntity;
import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.products.IProduct;

public interface ICategory extends IEntity{
	
	
	public String getCategoryCode();
	
	public String getCategoryId();
	
		
	public String getCategoryName();

	public void setCategoryName(String categoryName);
	
	public CatalogJPA getCatalog();

	public void setCatalog(CatalogJPA catalog);
	
	public void addProduct(IProduct product);
	
	public IProduct getProduct(String id);
	
	public IProduct getChildByProductCode(Integer name);
	
	public Date getCreationDate();

	public void setCreationDate(Date creationDate);

	public Date getModifiedDate();

	public void setModifiedDate(Date modifiedDate);

}
