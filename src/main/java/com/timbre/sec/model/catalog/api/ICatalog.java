package com.timbre.sec.model.catalog.api;

import com.timbre.common.IEntity;

public interface ICatalog extends IEntity{
	
	public String getCatalogId();
	
	public void setCatalogId(String catId);
	
	public void setCatalogCode(String name);
	
	public String getCatalogCode();
	
	

}
