package com.timbre.sec.model.category;


import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.timbre.common.IEntity;

/*import com.timbre.persistence.jpa.model.BooleanAttributeJPA;
import com.timbre.persistence.jpa.model.CollectionItemDetailsJPA;
import com.timbre.persistence.jpa.model.CollectionItemJPA;
import com.timbre.persistence.jpa.model.IntegerAttributeJPA;
import com.timbre.persistence.jpa.model.ItemJPA;
import com.timbre.persistence.jpa.model.QNameJPA;*/
import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;

//import com.timbre.sec.model.shopping.impl.ShoppingCartJPA;

@Entity
//@DiscriminatorValue("collection")
//@Table(name = "category")
public class CategoryJPA implements ICategory { // cant extend extends ItemJPA

	private static final long serialVersionUID = 2873258323314048223L;

	// CollectionItem specific attributes
	/*public static final QName ATTR_EXCLUDE_FREE_BUSY_ROLLUP =
		new QNameJPA(CollectionItem.class, "excludeFreeBusyRollup");

	public static final QName ATTR_HUE =
		new QNameJPA(CollectionItem.class, "hue");*/
	
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	// The factory generates the id factory.generateUid()
	@Column(name = "catalogId", nullable=false, unique=false, length=255)
	@NotNull
	@org.hibernate.validator.constraints.Length(min=1, max=255)
	@Index(name="idx_catalogId")
	public String catalogId;
	
	
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
	
	@Id
	public String categoryId;
	
	protected UUID idOne;
	
	public CategoryJPA(){
		super();
		this.setCategoryId(generateUid());
	}
	
	public String generateUid() {
    	idOne = UUID.randomUUID();
        return String.valueOf(idOne);
    }

	public void setCategoryId(String cid){
		
		this.categoryId = cid;
	}
	
	public String getCategoryId(){
		
		return categoryId;
	}
	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	@JsonIgnore
	@OneToMany(targetEntity=ProductJPA.class, mappedBy="categoryJPA", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	//@Cascade( {CascadeType.DELETE }) 
	private Set<IProduct> products = new HashSet<IProduct>(0);



	private transient Set<IProduct> children = null;

	//*********** ShoppingCartItem Specific stuff **************/
		
	private String categoryCode;

	
	private String categoryName;



	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@JoinColumn(name = "catalogjpa", referencedColumnName = "catalogid")

	@ManyToOne(optional = false)
	private CatalogJPA catalogJPA;

	//private ShoppingCartForComplexItemsJPA ShoppingCartForComplexItemsJPA;

	public CatalogJPA getCatalog() {
		return catalogJPA;
	}

	public void setCatalog(CatalogJPA catalog) {
		this.catalogJPA = catalog;
	}

	
	public void addProduct(IProduct product)
	{
		//See if there's already an item like this in the cart
		boolean currIndex = products.isEmpty();	
		if (currIndex) {
			//If the item is new, add it to the cart
			products.add(product);
		} else {
			Boolean itemExists=false;
			for (Iterator<IProduct> it = products.iterator (); it.hasNext (); ) {
			    Object o = it.next ();
			    ProductJPA p =(ProductJPA)o;
			    //if(p.getProductCode() == p.getProductCode()){
			    	if(product.getProductId() == p.getProductId()){	
			    	//update
			    	//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
			    	itemExists=true;
			    }
			  }
			if(!itemExists)
				products.add(product);

			
		}
	}
	
	
	// this might be changed to a String
	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	
	

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( categoryName == null ) ? 0 : categoryName.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final CategoryJPA other = (CategoryJPA) obj;
		if( categoryName == null ){
			if( other.categoryName != null )
				return false;
		}
		else if( !categoryName.equals( other.categoryName ) )
			return false;
		return true;
	}

	/*@Override
	public String toString(){
		return new ToStringBuilder( this ).append( "id", id ).append( "name", name ).toString();
	}*/

	

	/* (non-Javadoc)
	 * @see org.osaf.cosmo.model.CollectionItem#getChildren()
	 */
	/*public Set<Item> getChildren() {
		if(children!=null)
			return children;

		children = new HashSet<Item>();
		for(CollectionItemDetails cid: childDetails)
			children.add(cid.getItem());

		children = Collections.unmodifiableSet(children);

		return children;
	}*/
	
	public Set<IProduct> getProducts() {
		if(products!=null)
			return products;

		children = new HashSet<IProduct>();
		for(IProduct prod: products)
			children.add(prod);

		children = Collections.unmodifiableSet(children);

		return children;
	}

	/* (non-Javadoc)
	 * @see org.osaf.cosmo.model.CollectionItem#getChildDetails(org.osaf.cosmo.model.Item)
	 */
	
	
	/*public CollectionItemDetails getChildDetails(Item item) {
		for(CollectionItemDetails cid: childDetails)
			if(cid.getItem().equals(item))
				return cid;

		return null;
	}*/

	/* (non-Javadoc)
	 * @see org.osaf.cosmo.model.CollectionItem#getChild(java.lang.String)
	 */
	public IProduct getProduct(String uid) {
		for (IProduct child : getProducts()) {
			if (child.getProductId().equals(uid))
				return child;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.osaf.cosmo.model.CollectionItem#getChildByName(java.lang.String)
	 */
	public IProduct getChildByProductCode(Integer name) {
		for (IProduct child : getProducts()) {
			if (child.getProductCode().equals(name))
				return child;
		}
		return null;
	}

	
	/*public boolean isExcludeFreeBusyRollup() {
		Boolean bv =  BooleanAttributeJPA.getValue(this, ATTR_EXCLUDE_FREE_BUSY_ROLLUP);
		if(bv==null)
			return false;
		else
			return bv.booleanValue();
	}

	
	public void setExcludeFreeBusyRollup(boolean flag) {
		BooleanAttributeJPA.setValue(this, ATTR_EXCLUDE_FREE_BUSY_ROLLUP, flag);
	}

	
	public Long getHue() {
		return IntegerAttributeJPA.getValue(this, ATTR_HUE);
	}

	
	public void setHue(Long value) {
		IntegerAttributeJPA.setValue(this, ATTR_HUE, value);
	}

	
	public boolean removeTombstone(Item item) {
        ItemTombstone ts = new ItemTombstoneJPA(this, item);
        return tombstones.remove(ts);
    }

	
	public int generateHash() {
		return getVersion();
	}
	*/

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/*public Item copy() {
		CollectionItem copy = new CollectionItemJPA();
		copyToItem(copy);
		return copy;
	}*/

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}


}

