package com.timbre.sec.model.web.content;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.timbre.common.IEntity;

import com.timbre.sec.model.products.IDescription;
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;


@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
//public class DescriptionJPA  extends AuditableObjectJPA implements IDescription , Serializable{
public class DescriptionJPA  implements IEntity,  Serializable{
	private static final long serialVersionUID = 1L;

	
	/*CREATE TABLE `documents` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `productCode` varchar(200) NOT NULL,
			  `prodDesc` text NOT NULL,
			  `filename` varchar(200) NOT NULL,
			  `content` mediumblob NOT NULL,  for ORACLE enter BLOB
			  `content_type` varchar(255) NOT NULL,
			  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`)
			);*/
	
	
	//@Id
    /*@GeneratedValue
    @Column(name="id")
    private Long id;*/
 
    @Column(name="productCode")
    private String productCode;
 
        
    @Id
    @Column(name="productId")
    private String productId;
    
    private String descriptionId;
    
    

    
    public String getDescriptionId() {
		return descriptionId;
	}

	public void setDescriptionId(String descriptionId) {
		this.descriptionId = descriptionId;
	}
	
	@JsonIgnore
	@OneToOne
	private ProductJPA productJPA;
	
    public ProductJPA getProductJPA() {
		return productJPA;
	}

	public void setProductJPA(ProductJPA productJPA) {
		this.productJPA = productJPA;
	}

	@Lob
    @Column(name="prodDesc", length=1048576)
	private String prodDesc;
	
	@Lob
    @Column(name="fabricContent", length=1048576)
	private String fabricContent;
	
	@Lob
    @Column(name="fabricProperties", length=1048576)
	private String fabricProperties;
	
	@Lob
    @Column(name="gramentConstruction", length=1048576)
	private String gramentConstruction;
	
	@Lob
    @Column(name="designHighlights", length=1048576)
	private String designHighlights;
	
	@Lob
    @Column(name="generalFit", length=1048576)
	private String generalFit;
	
	@Lob
    @Column(name="generalLength", length=1048576)
	private String generalLength;
	
	
	@JsonIgnore
    @Column(name="filename")
    private String filename;
 
    /*@Column(name="content")
    @Lob
    private Blob content;*/
     
    @Column(name="content_type")
    private String contentType;
     
    @Column(name="created")
    private Date created;
    
    protected UUID idOne;
	public DescriptionJPA()
	{
		this.setDescriptionId(generateUid());
	}
	public String generateUid() {
		idOne = UUID.randomUUID();
		return String.valueOf(idOne);
	}

	public void setProductCode(String name) {
		this.productCode = name;
	}
	
	public String getProductCode(){
		return productCode;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String description) {
		this.prodDesc = description;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	/*public Blob getContent() {
		return content;
	}

	public void setContent(Blob content) {
		this.content = content;
	}*/

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
		
	
	/*@Override
	public Long getId() {
		
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}*/
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( productId == null ) ? 0 : productId.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final DescriptionJPA other = (DescriptionJPA) obj;
		if( descriptionId == null ){
			if( other.descriptionId != null )
				return false;
		}
		else if( !descriptionId.equals( other.descriptionId ) )
			return false;
		return true;
	}

	
	/*@Override
	public void setDescription(String descript) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DescriptionJPA getDescription() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFabricContent(String fabricContent) {
		this.fabricContent = fabricContent;
	}

	public String getFabricContent() {
		return fabricContent;
	}

	public void setFabricProperties(String fabricProperties) {
		this.fabricProperties = fabricProperties;
	}

	public String getFabricProperties() {
		return fabricProperties;
	}

	public void setGramentConstruction(String gramentConstruction) {
		this.gramentConstruction = gramentConstruction;
	}

	public String getGramentConstruction() {
		return gramentConstruction;
	}

	public void setDesignHighlights(String designHighlights) {
		this.designHighlights = designHighlights;
	}

	public String getDesignHighlights() {
		return designHighlights;
	}

	public void setGeneralFit(String generalFit) {
		this.generalFit = generalFit;
	}

	public String getGeneralFit() {
		return generalFit;
	}

	public void setGeneralLength(String generalLength) {
		this.generalLength = generalLength;
	}

	public String getGeneralLength() {
		return generalLength;
	}

	

	
	

}
