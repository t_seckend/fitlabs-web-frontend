package com.timbre.sec.model.web.content;

import java.util.Iterator;
import java.util.Set;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Embeddable
public class ColorsJPA {
	
	Set<String> colors;

	public Set getColors() {
		return colors;
	}

	public void setColors(Set colors) {
		this.colors = colors;
	}
	
	public void addNewColorItem(String newItem)
	{
		//See if there's already an item like this in the cart
		boolean currIndex = colors.isEmpty();	
		if (currIndex) {
			//If the item is new, add it to the cart
			colors.add(newItem);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = colors.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String color =(String)o;
				if(color == newItem){
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				colors.add(newItem);


		}
	}
	
	

}
