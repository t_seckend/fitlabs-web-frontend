package com.timbre.sec.model.web.content;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


import javax.imageio.ImageIO;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import org.primefaces.model.DefaultStreamedContent;
//import org.primefaces.model.StreamedContent;

import com.timbre.common.IEntity;
import com.timbre.sec.model.products.IGallery;
import com.timbre.sec.model.products.ProductJPA;

@Entity
public class GalleryJPA implements IEntity,  Serializable{
	
	private static final long serialVersionUID = 1L;
	
	

	
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	
	
	private String MimeType;  // Useful to store the mime type incase you want to send it back via a servlet.
	
	
	private String productCode;
	
	private String icon;
	
	@Id
	private String productId;
	
	private String imageUrl;
	
	
	
	//@Transient
	@ElementCollection(fetch=FetchType.EAGER)
	public Map<String,String> iconToPicMap;
	
	@JsonIgnore
	@OneToOne
	private ProductJPA productJPA;
	
	private String galleryId;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> iconList;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> imageDetailsList;
	
	

	public List<String> getImageDetailsList() {
		return imageDetailsList;
	}

	public void setImageDetailsList(List<String> imageDetailsList) {
		this.imageDetailsList = imageDetailsList;
	}

	public List<String> getIconList() {
	    return new ArrayList<String>(iconList);
	}
	
	/*public Collection<String> getIconList() {
		return iconList;
	}*/

	public void setIconList(HashSet<String>  iconList) {
		this.iconList = iconList;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> imageList;
	
	public Collection<String> getImageList() {
		return imageList;
	}

	public void setImageList(HashSet<String> imageList) {
		this.imageList = imageList;
	}
	
	
	
	@Transient
	private List<String> selectedIcon; 
	
	public List<String> getSelectedIcon() {
		return selectedIcon;
	}

	public void setSelectedIcon(List<String> selectedIcon) {
		this.selectedIcon = selectedIcon;
	}
	
	
	
	public String getIcon() {
		if(selectedIcon == null){
			return icon;
		}
		// Quantities is an array in ShoppingCart so cant go through that
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	// Can Remove after the Database is working
	/*public void iconValueChange(AjaxBehaviorEvent event){
		
		System.out.println("icon value change in gallery value :"+event.getSource().toString());
		//event.getComponent().getAttributes().values();
		
		for (Iterator<Object> it = event.getComponent().getAttributes().values().iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String value =(String)o;
				
					System.out.println("IconList had a icon, not added :"+value);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					
			}
	}*/
	
	@JsonIgnore
	@Lob
	@Basic(fetch=FetchType.EAGER) // this gets ignored anyway, but it is recommended for blobs
    private byte[] image;
	
	public byte[] getImage() {
		return image;
	}
	
	
	// Can Remove after the Database is working
	/*@Transient
	private StreamedContent productPic;
	
	public StreamedContent getProductPic()throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            //String studentId = context.getExternalContext().getRequestParameterMap().get("studentId");
            //Student student = studentService.find(Long.valueOf(studentId));
            return new DefaultStreamedContent(new ByteArrayInputStream(getImage()));
        }
    }*/


	public void setImage(byte[] image) {
		this.image = image;
	}
	
	private String imageName;
	
	
	protected UUID idOne;
	
	public GalleryJPA()
	{
		this.setGalleryId(generateUid());
		iconToPicMap = new HashMap<String, String>();
		
	}
	
	
	/*
	12100010 (SteetHeart Tank)
	12100110 (SteetHeart Tee)
	12100310 (SteetHeart 3/4Slve)
	12203010 (Botm Short ColorSplash)
	12204110 (Botm Capri ColorSplash)
	12205210 (Botm Pant ColorSplash)
	12207210 (Botm Pant Basic)

	Color: Torq  Purple  Raspbery
	Size: 1X  2x  3x  4X*/
	    
	
	
	public void buildIconToPicMap()
	{
		
		String iC = null;
		String iM = null;
		String colorIdentifier = null;
		
		if(iconToPicMap == null)
		iconToPicMap = new HashMap<String, String>();
		
		
		
		//Iterator<String> itOverIcons = iconList.iterator();
		Iterator<String> itOverImages = imageList.iterator();
		
		while(itOverImages.hasNext()){
			
			iM = itOverImages.next();
			colorIdentifier = Character.toString(iM.charAt(8));
						
		if(colorIdentifier.equalsIgnoreCase("T")){
			
			Iterator<String> ityForTurq = iconList.iterator();
			
			while(ityForTurq.hasNext()){
			
				iC = ityForTurq.next();
				if(iC.equalsIgnoreCase("turqIcon")){
					iconToPicMap.put(iC, iM);
					
				} 
							
			}
			
		} else if(colorIdentifier.equalsIgnoreCase("P")){
			
			Iterator<String> ityForPurple = iconList.iterator();
			
			while(ityForPurple.hasNext()){
				
				iC = ityForPurple.next();
				if(iC.equalsIgnoreCase("purpleIcon")){
					iconToPicMap.put(iC, iM);
					
				} 
							
			}
			
			
		
		} else if(colorIdentifier.equalsIgnoreCase("F")){
			
			Iterator<String> ityForPink = iconList.iterator();
			
			while(ityForPink.hasNext()){
				
				iC = ityForPink.next();
				if(iC.equalsIgnoreCase("pinkIcon")){
					iconToPicMap.put(iC, iM);
					
				} 
							
			}
			
		}  else if(colorIdentifier.equalsIgnoreCase("B")){
			
			Iterator<String> ityForBlack = iconList.iterator();
			
			while(ityForBlack.hasNext()){
				
				iC = ityForBlack.next();
				if(iC.equalsIgnoreCase("blackIcon")){
					iconToPicMap.put(iC, iM);
					
				} 
							
			}
			
		}
		}
		
	}
	
	
	public String generateUid() {
		idOne = UUID.randomUUID();
		return String.valueOf(idOne);
	}
	
	public void addNewImageItem(String image){
		
		//See if there's already an item like this in the cart
		if(this.imageList == null)
			this.imageList = new HashSet();
		boolean empty = imageList.isEmpty();	
		if (empty) {
			System.out.println("ImageList was empty so adding :"+image);
			//If the item is new, add it to the cart
			imageList.add(image);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = imageList.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String img =(String)o;
				if(img == image){
					System.out.println("Image had a image, not added :"+image);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				System.out.println("Image did not have a image, added :"+image);
			imageList.add(image);


		}
		
	}
	
public void addNewImageDetailItem(String image){
		
		//See if there's already an item like this in the cart
		if(this.imageDetailsList == null)
			this.imageDetailsList = new ArrayList<String>();
		boolean empty = imageDetailsList.isEmpty();	
		if (empty) {
			System.out.println("ImageList was empty so adding :"+image);
			//If the item is new, add it to the cart
			imageDetailsList.add(image);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = imageDetailsList.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String img =(String)o;
				if(img == image){
					System.out.println("Image had a image, not added :"+image);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				System.out.println("Image did not have a image, added :"+image);
			imageDetailsList.add(image);


		}
		
	}
	
	public void addNewIconItem(String icon) {
		
		//See if there's already an item like this in the cart
		if(this.iconList == null)
			this.iconList = new HashSet();
		boolean empty = iconList.isEmpty();	
		if (empty) {
			System.out.println("IconList was empty so adding :"+icon);
			//If the item is new, add it to the cart
			iconList.add(icon);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = iconList.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String icn =(String)o;
				if(icn == icon){
					System.out.println("IconList had a icon, not added :"+icon);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				System.out.println("IconList did not have a image, added :"+icon);
			iconList.add(icon);


		}
		
	}
	
	public String getImageName() {
		return imageName;
	}


	public String getMimeType() {
		return MimeType;
	}


	public void setMimeType(String mimeType) {
		MimeType = mimeType;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	

	@Column( unique = false, nullable = true )
    private String img;
	
	@Column( unique = false, nullable = true )
    private String thumb;
	
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String name) {
		this.productCode = name;
	}


	public ProductJPA getProductJPA() {
		return productJPA;
	}


	public void setProductJPA(ProductJPA productJPA) {
		this.productJPA = productJPA;
	}


	public String getImageUrl() {
		return imageUrl;
	}


	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getGalleryId() {
		return galleryId;
	}
	public void setGalleryId(String galleryId) {
		this.galleryId = galleryId;
	}
	
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( galleryId == null ) ? 0 : galleryId.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final GalleryJPA other = (GalleryJPA) obj;
		if( galleryId == null ){
			if( other.galleryId != null )
				return false;
		}
		else if( !galleryId.equals( other.galleryId ) )
			return false;
		return true;
	}
	/* @Override
	public Date getCreationDate() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Date getModifiedDate() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void updateTimestamp() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getEntityTag() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setProductPic(StreamedContent productPic) {
		this.productPic = productPic;
	} 
	*/
	
	

}


