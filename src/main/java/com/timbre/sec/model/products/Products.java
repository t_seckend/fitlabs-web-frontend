package com.timbre.sec.model.products;
/*package com.timbre.sec.model.products;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;

import com.timbre.rest.common.IEntity;
import com.timbre.rest.sec.model.MyUserDetails;
import com.timbre.sec.model.shopping.order.Order;
import com.timbre.rest.sec.model.web.content.Gallery;

import java.math.BigDecimal;
import java.util.List;

@Entity
public class Products implements IProducts, IEntity{
	
	
	 * DROP TABLE IF EXISTS products;
CREATE TABLE products (
  products_id bigint NOT NULL auto_increment,
  products_type int(11) NOT NULL default '1',
  products_quantity float NOT NULL default '0',
  products_model varchar(32) default NULL,
  products_image varchar(64) default NULL,
  products_price decimal(15,4) NOT NULL default '0.0000',
  products_virtual tinyint(1) NOT NULL default '0',
  products_date_added datetime NOT NULL default '0001-01-01 00:00:00',
  products_last_modified datetime default NULL,
  products_date_available datetime default NULL,
  products_weight float NOT NULL default '0',
  products_status tinyint(1) NOT NULL default '0',
  products_tax_class_id int(11) NOT NULL default '0',
  manufacturers_id int(11) default NULL,
  products_ordered float NOT NULL default '0',
  products_quantity_order_min float NOT NULL default '1',
  products_quantity_order_units float NOT NULL default '1',
  products_priced_by_attribute tinyint(1) NOT NULL default '0',
  product_is_free tinyint(1) NOT NULL default '0',
  product_is_call tinyint(1) NOT NULL default '0',
  products_quantity_mixed tinyint(1) NOT NULL default '0',
  product_is_always_free_shipping tinyint(1) NOT NULL default '0',
  products_qty_box_status tinyint(1) NOT NULL default '1',
  products_quantity_order_max float NOT NULL default '0',
  products_sort_order int(11) NOT NULL default '0',
  products_discount_type tinyint(1) NOT NULL default '0',
  products_discount_type_from tinyint(1) NOT NULL default '0',
  products_price_sorter decimal(15,4) NOT NULL default '0.0000',
  master_categories_id int(11) NOT NULL default '0',
  products_mixed_discount_quantity tinyint(1) NOT NULL default '1',
  metatags_title_status tinyint(1) NOT NULL default '0',
  metatags_products_name_status tinyint(1) NOT NULL default '0',
  metatags_model_status tinyint(1) NOT NULL default '0',
  metatags_price_status tinyint(1) NOT NULL default '0',
  metatags_title_tagline_status tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (products_id),
  KEY idx_products_date_added_zen (products_date_added),
  KEY idx_products_status_zen (products_status),
  KEY idx_products_date_available_zen (products_date_available),
  KEY idx_products_ordered_zen (products_ordered),
  KEY idx_products_model_zen (products_model),
  KEY idx_products_price_sorter_zen (products_price_sorter),
  KEY idx_master_categories_id_zen (master_categories_id),
  KEY idx_products_sort_order_zen (products_sort_order),
  KEY idx_manufacturers_id_zen (manufacturers_id)
) ENGINE=MyISAM;
	 
	
	
	
	
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long productId;

	@Column( unique = false,nullable = false )
	private String name;
    
	@Column( unique = false,nullable = false )
    private float price;
	
	@Column( unique = false,nullable = false )
    private String catName;

	@Column( unique = false,nullable = false )
    private float weight;

	@Column( unique = false,nullable = false )
    private float length;

	@Column( unique = false,nullable = false )
    private float height;

	@Column( unique = false,nullable = false )
    private boolean isPresent;

	@Column( unique = false,nullable = false )
    private boolean foamed;

	@Column( unique = false,nullable = false )
    private int count;

	@Column( unique = false,nullable = false )
    private String imgPath;

	@Column( unique = false,nullable = false )
    private String videoUrl;

	@Column( unique = false,nullable = false )
    private String description;

	@OneToMany( fetch = FetchType.EAGER )
	@JoinColumn( name = "PRODUCT_ID" )
	@Column( nullable = true )
    private List<GalleryJPA> galleries;

        public Products() {
        	super();
        }
        
        @Override
		public void setId(Long id) {
			this.productId = id;
			
		}

        public float getLength() {
                return length;
        }

        public void setLength(float length) {
                this.length = length;
        }

        public float getHeight() {
                return height;
        }

        public void setHeight(float height) {
                this.height = height;
        }

        public Long getId() {
                return productId;
        }

       
        public BigDecimal getTotalPrice() {

                HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
                                .getCurrentInstance().getExternalContext().getResponse();

                BigDecimal subTotal = new BigDecimal(this.price * this.count).setScale(
                                1, BigDecimal.ROUND_HALF_DOWN);

                Cookie cookie = new Cookie("cart_total", String.valueOf(subTotal));
                cookie.setPath("/");
                cookie.setMaxAge(24 * 3600 * 14);
                httpServletResponse.addCookie(cookie);

                return subTotal;
        }

        public List<GalleryJPA> getGalleries() {
                return galleries;
        }

        public void setGalleries(List<GalleryJPA> galleries) {
                this.galleries = galleries;
        }

        public String getDescription() {

                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getImgPath() {
                return imgPath;
        }

        public void setImgPath(String imgPath) {
                this.imgPath = imgPath;
        }

        public int getCount() {
                return count;
        }

        public void setCount(int count) {
                this.count = count;
        }

        public boolean isPresent() {
                return isPresent;
        }

        public void setPresent(boolean isPresent) {
                this.isPresent = isPresent;
        }

        public float getWeight() {
                return weight;
        }

        public void setWeight(float weight) {
                this.weight = weight;
        }

        public String getCatName() {
                return catName;
        }

        public void setCatName(String catName) {
                this.catName = catName;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public float getPrice() {
                return price;
        }

        public void setPrice(float price) {
                this.price = price;
        }

        public boolean isFoamed() {
                return foamed;
        }

        public void setFoamed(boolean foamed) {
                this.foamed = foamed;
        }

        public String getVideoUrl() {
                return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
                this.videoUrl = videoUrl;
        }

        public boolean equals(Object o) {
                final Products other = (Products) o;

                if (this.name.equals(other.getName())) {
                        return true;
                }

                return false;
        }

        @Override
        public int hashCode() {
                // hash code is product name oriented as far names are unique
                final int PRIME = 31;
                int result = 1;
                result = PRIME * result + name.hashCode();
                return result;
        }

		@Override
		public void save(Order order) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public List<Products> search(String param) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<GalleryJPA> getGallery(String param) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Products getProductById(String param) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<Comment> getComments(String param) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void save(Comment comment) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void registerNewUser(MyUserDetails user) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getUserCity(String username) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getUserMail(String username) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int confirmUser(String username, String confirmationCode) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean confirmed(String username) {
			// TODO Auto-generated method stub
			return false;
		}

		

        // public boolean equals(Object o) {
        // return EqualsBuilder.reflectionEquals(this, o);
        // }
        //
        // public int hashcode() {
        // return HashCodeBuilder.reflectionHashCode(this);
        // }

}
*/