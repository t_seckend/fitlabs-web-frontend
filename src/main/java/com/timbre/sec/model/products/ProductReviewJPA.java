package com.timbre.sec.model.products;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

//import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.timbre.common.IEntity;
import com.timbre.persistence.jpa.model.UserJPA;



@Entity
	public class ProductReviewJPA implements IEntity, IProductReview , Serializable {	
	private static final long serialVersionUID = 1L;
	
	@GeneratedValue
    private Long id;
	
	@JsonIgnore
	@JoinColumn(name = "productId", insertable=false, updatable=false)
	@ManyToOne(optional = false)
	private ProductJPA productJPA;
	
	@JoinColumn(name = "id", insertable=false, updatable=false  )
	@ManyToOne(optional = false)
	private UserJPA userJPA;
	
	@Id
	private String productId;
	
		
	private String productReviewId;
	
	public String getProductReviewId() {
		return productReviewId;
	}

	public void setProductReviewId(String productReviewId) {
		this.productReviewId = productReviewId;
	}

	private String productCode;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productid) {
		this.productId = productid;
	}  
	
	
	public ProductJPA getProductJPA() {
		return productJPA;
	}

	public void setProductJPA(ProductJPA product) {
		this.productJPA = product;
	}

	public Integer rating = 0;
	
	@Transient	
	public  String overallRatings[] = new String[]{"1", "2","3", "4", "5"};
	@Transient
	public  String ageDemograpic[] = new String[]{"Under 25", "25-35","35-45", "45-55", "Over 55"};
	
	public boolean reviewHelpfull;
	
	public int ageDemo = 0;
	
	public String reviewOwner = null;
	
	public String displayName = null;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
    public Date creationDate;
	
		
	@Lob
    public String review;
	
	protected UUID idOne;
	
	public ProductReviewJPA(){
		super();
		this.setProductReviewId(generateUid());
	}
	
	public String generateUid() {
    	idOne = UUID.randomUUID();
        return String.valueOf(idOne);
    }
	
	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String getProductCode() {
		
		return productCode;
	}

	@Override
	public void setProductCode(String code) {
		this.productCode = code;
		
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( productReviewId == null ) ? 0 : productReviewId.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final ProductReviewJPA other = (ProductReviewJPA) obj;
		if( productReviewId == null ){
			if( other.productReviewId != null )
				return false;
		}
		else if( !productReviewId.equals( other.productReviewId ) )
			return false;
		return true;
	}

	@Override
	public void setProductRating(Integer rate) {
		this.rating = rate;
		
	}

	@Override
	public Integer getProductRating() {
		
		return rating;
	}

	public void setReviewHelpfull(boolean reviewHelpfull) {
		this.reviewHelpfull = reviewHelpfull;
	}

	public boolean isReviewHelpfull() {
		return reviewHelpfull;
	}

	public void setAgeDemo(int ageDemo) {
		this.ageDemo = ageDemo;
	}

	public int getAgeDemo() {
		return ageDemo;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public String[] getOverallRatings() {
		return overallRatings;
	}

	public void setOverallRatings(String[] overallRatings) {
		this.overallRatings = overallRatings;
	}

	public void setReviewOwner(String reviewOwner) {
		this.reviewOwner = reviewOwner;
	}

	public String getReviewOwner() {
		return reviewOwner;
	}

	public String[] getAgeDemograpic() {
		return ageDemograpic;
	}

	public void setAgeDemograpic(String[] ageDemograpic) {
		this.ageDemograpic = ageDemograpic;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setUserJPA(UserJPA userJPA) {
		this.userJPA = userJPA;
	}

	public UserJPA getUserJPA() {
		return userJPA;
	}
	

}
