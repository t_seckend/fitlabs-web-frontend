package com.timbre.sec.model.products;
import com.timbre.common.AuditableObject;


public interface IProductReview {
	
	
	

		public String getProductId();

		public String getProductCode();
		
		public void setProductCode(String code);
		
		public ProductJPA getProductJPA() ;

		public void setProductJPA(ProductJPA product);
		
		public void setProductRating(Integer rate);
		
		public Integer getProductRating();

		public String getProductReviewId();

	

}
