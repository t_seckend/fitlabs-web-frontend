package com.timbre.sec.model.products;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;

import javax.annotation.PostConstruct;
/*import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;*/
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Target;
/*import org.primefaces.context.RequestContext;
import org.primefaces.event.RateEvent;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ComparisonChain;
import com.timbre.common.IEntity;

import com.timbre.sec.model.catalog.CatalogJPA;
import com.timbre.sec.model.catalog.api.ICategory;
import com.timbre.sec.model.category.CategoryJPA;

import com.timbre.sec.model.web.content.ColorsJPA;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;
import com.timbre.sec.persistence.service.IGalleryService;
import com.timbre.sec.persistence.service.IProductReviewService;
import com.timbre.sec.persistence.service.impl.ProductServiceImpl;



@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//public class ProductJPA extends AuditableObjectJPA implements IProduct , Serializable{ 
	public class ProductJPA implements IEntity, IProduct,  Serializable{ 
	private static final Logger logger = LoggerFactory.getLogger(ProductJPA.class);
	private static final long serialVersionUID = 1L;
	
	@Transient
	public static  int quantities[] = new int[]{1, 2, 3, 4, 5};
	
	static {
		
		quantities = new int[]{1, 2, 3, 4, 5};
		
		
	}
	  
	  private String name;
	  
	  private String price;
	  
	  private int discount = 0;
	  
	  private String size;
	  
	  private String color;
	  
	  private int quantity = 1;
	  
	  private int overallRating = 1;
	  
	  private int rating = 1;
	  
	  private String styleNumber;
	  
	  private ProductReviewJPA reviewJPA;
	  
	  private String sizeDescript = "X";
	
	  /*private String productsImage1 = null;
	  private String productsImage2 = null;
	  private String productsImage3 = null;
	  private String productsImage4 = null;*/

	public String getSizeDescript() {
		return sizeDescript;
	}

	public void setSizeDescript(String sizeDescript) {
		this.sizeDescript = sizeDescript;
	}
	
	@JsonIgnore
	private String categoryId;
	  
	  
	  // we generate this value in the constructor
	  @Id
	  public String productId;
	  
	  private String productCode;
		
		
		public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

		protected UUID idOne;
		
		public ProductJPA(){
			super();
			this.setProductId(generateUid());
			
			
			 
		}  
		
		public String generateUid() {
	    	idOne = UUID.randomUUID();
	        return String.valueOf(idOne);
	    }

		public void setProductId(String pid){
			
			this.productId = pid;
		}  
	  
	  public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryid) {
		this.categoryId = categoryid;
	}
	
	
	
	// This should be a real Angular2 event
	public void createReview(String event){
		reviewJPA = new ProductReviewJPA();
		reviewJPA.setProductId(productId);
		reviewJPA.setProductJPA(this);
	}
	
	 
	
	public int[] getQuantities() {
		return quantities;
	}

	public void setQuantities(int[] quantities) {
		this.quantities = quantities;
	}
	
	
	
	public int getQuantity() {
		
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	@JsonIgnore
	@Transient
	private List<String> selectedColors; 
	
	public List<String> getSelectedColors() {
		return selectedColors;
	}

	public void setSelectedColors(List<String> selectedColors) {
		this.selectedColors = selectedColors;
	}
	
	
	
	public String getColor() {
		if(selectedColors == null){
			return color;
		}
		for(String s : colors){
			System.out.println("color in Selected colors :"+s);
			return color = s;
		}
		
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}


	@JsonIgnore
	@Transient
	private List<String> selectedSizes;

	@SuppressWarnings("unchecked")
	public List<String> getSelectedSizes() {
		
		Collections.sort(selectedSizes, new com.timbre.sec.util.NaturalOrderComparator<String>(false) );		
		//return asSortedList(selectedSizes);//selectedSizes;
		return selectedSizes;
	}

	public void setSelectedSizes(List<String> selectedSizes) {
		this.selectedSizes = selectedSizes;
	}
	
	public static
	<T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
	  List<T> list = new ArrayList<T>(c);
	  java.util.Collections.reverse(list);
	  
	  return list;
	}
	
	

	public String getSize() {
		if(selectedSizes == null){
			return size;
		}
		for(String s : sizes){
			System.out.println("size in Selected Sizes :"+s);
			return size = s;
		}
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}



	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> colors;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> sizes;
	
	
	
	public Collection<String> getSizes() {
		return this.sizes;
	}
	/*public Collection<String> getSizes() {
		//return asSortedList(sizes);
		
		
		List<String> list = new ArrayList<String>(sizes);
		  //java.util.Collections.reverse(list);
		  Collections.sort(list, new Comparator<String>(){    
		         @Override 
		         public int compare(String e1, String e2) {
		            return ComparisonChain.start()  
		                 .compare(e1.charAt(0), e2.charAt(0)).result(); 
		    }});
		
		
		return list;
	}*/

	public void setSizes(Set<String> sizes) {
		this.sizes = sizes;
	}

	public Collection<String> getColors() {
		return colors;
	}

	public void setColors(HashSet<String> colors) {
		this.colors = colors;
	}

	@OneToOne(targetEntity=GalleryJPA.class, mappedBy="productJPA", cascade=CascadeType.ALL, fetch = FetchType.EAGER)  
	private GalleryJPA gallery;
	
	   
	public GalleryJPA getGallery() {
		return gallery;
	}

	public void setGallery(GalleryJPA gall) {
		this.gallery = gall;
	}
	
	@OneToOne(targetEntity=DescriptionJPA.class, mappedBy="productJPA", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private DescriptionJPA description;

	public DescriptionJPA getDescription() {
		return description;
	}

	public void setDescription(DescriptionJPA description) {
		this.description = description;
	}

	@JsonIgnore
	@JoinColumn(name = "categoryId", insertable=false, updatable=false  )
	@ManyToOne(optional = false)
	private CategoryJPA categoryJPA;

	@JsonIgnore
	public CategoryJPA getCategory() {
		return categoryJPA;
	}
	@JsonIgnore
	public void setCategory(CategoryJPA category) {
		this.categoryJPA = category;
	}
	
	
	
	@OneToMany(targetEntity=ProductReviewJPA.class, mappedBy = "productJPA",  cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	 
	private Set <IProductReview> reviews = new HashSet<IProductReview>(0);
	
	
	
	
	
	@Transient
	private List<ProductReviewJPA> filteredReviews;
	
	@Transient
	private List<ProductReviewJPA> smallReviews;
	
	
	
	public void addProductReview(IProductReview newItem)
	{
		//See if there's already an item like this in the cart
		boolean currIndex = reviews.isEmpty();	
		if (currIndex) {
			//If the item is new, add it to the cart
			reviews.add(newItem);
		} else {
			boolean itemExists=false;
			for (Iterator<IProductReview> it = reviews.iterator (); it.hasNext(); ) {
				Object o = it.next ();
				ProductReviewJPA rev =(ProductReviewJPA)o;
				if(rev.getProductReviewId() == newItem.getProductReviewId()){
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				reviews.add(newItem);


		}
	}
	  
	  
	  
	  
	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public String getProductId() {
		
		return productId;
	}
	
	public String getStyleNumber() {
		return styleNumber;
	}

	public void setStyleNumber(String styleNumber) {
		this.styleNumber = styleNumber;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getCreationDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getModifiedDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateTimestamp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getEntityTag() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( productId == null ) ? 0 : productId.hashCode() );
		return result;
	}
	@Override
	public boolean equals( final Object obj ){
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		final ProductJPA other = (ProductJPA) obj;
		if( productId == null ){
			if( other.productId != null )
				return false;
		}
		else if( !productId.equals( other.productId ) )
			return false;
		return true;
	}

	public void addNewColorItem(String newItem)
	{
		//See if there's already an item like this in the cart
		if(this.colors == null)
			this.colors = new HashSet();
		boolean empty = colors.isEmpty();	
		if (empty) {
			System.out.println("colors was empty so adding :"+newItem);
			//If the item is new, add it to the cart
			colors.add(newItem);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = colors.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String color =(String)o;
				if(color == newItem){
					System.out.println("color had a newItem, not added :"+newItem);
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				System.out.println("color did not have a newItem, added :"+newItem);
				colors.add(newItem);


		}
	}
	
	public void addNewSize(String newSize)
	{
		//See if there's already an item like this in the cart
		if(this.sizes == null)
			this.sizes = new HashSet();
		boolean currIndex = sizes.isEmpty();	
		if (currIndex) {
			//If the item is new, add it to the cart
			sizes.add(newSize);
		} else {
			Boolean itemExists=false;
			for (Iterator<String> it = sizes.iterator (); it.hasNext (); ) {
				Object o = it.next ();
				String siz =(String)o;
				if(siz == newSize){
					// Need to implement an update 
					//shop.update(newItem.getItemCode(), newItem.getQty(), newItem.getPerItemCost());
					itemExists=true;
				}
			}
			if(!itemExists)
				sizes.add(newSize);


		}
	}
	
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOverallRating(int overallRating) {
		this.overallRating = overallRating;
	}

	public int getOverallRating() {
		return overallRating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getRating() {
		return rating;
	}

	public Set<IProductReview> getReviews() {
		return reviews;
	}

	public void setReviews(Set<IProductReview> reviews) {
		this.reviews = reviews;
	}

	public void setFilteredReviews(List<ProductReviewJPA> filteredReviews) {
		this.filteredReviews = filteredReviews;
	}

	public List<ProductReviewJPA> getFilteredReviews() {
		return filteredReviews;
	}

	public void setSmallReviews(List<ProductReviewJPA> smallReviews) {
		this.smallReviews = smallReviews;
	}

	public List<ProductReviewJPA> getSmallReviews() {
		return smallReviews;
	}

	

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getDiscount() {
		return discount;
	}

	/*public String getProductsImage1() {
		return productsImage1;
	}

	public void setProductsImage1(String productsImage1) {
		this.productsImage1 = productsImage1;
	}

	public String getProductsImage2() {
		return productsImage2;
	}

	public void setProductsImage2(String productsImage2) {
		this.productsImage2 = productsImage2;
	}

	public String getProductsImage3() {
		return productsImage3;
	}

	public void setProductsImage3(String productsImage3) {
		this.productsImage3 = productsImage3;
	}

	public String getProductsImage4() {
		return productsImage4;
	}

	public void setProductsImage4(String productsImage4) {
		this.productsImage4 = productsImage4;
	}*/
	
	
	
	/*
	 * 
	 productType 
			  productQuantity 
			  productModel 
			  productsImage 
			  productsPrice 
			  productsVirtual 
			  productDateAdded 
			  productLastModified 
			  productDateAvailable 
			  productWeight 
			  productStatus 
			  productTaxClassId 
			  manufacturerId 
			  productOrdered 
			  productQuantityOrderMin 
			  productQuantityOrderUnits 
			  productPricedByAttribute 
			  productIsFree 
			  productIsCall 
			  productQuantityMixed 
			  productIsAlwaysFreeShipping 
			  productQtyBoxStatus 
			  productQuantityOrderMax 
			  productSortOrder 
			  productDiscountType 
			  productDiscountTypeFrom 
			  productsPriceSorter 
			  masterCategoriesId 
			  productMixedDiscountQuantity 
			  metatagsTitleStatus 
			  metatagsProductsNameStatus 
			  metatagsModelStatus 
			  metatagsPriceStatus 
			  metatagsTitleTaglineStatus
			  KEY idx_products_date_added_zen (products_date_added),
			  KEY idx_products_status_zen (products_status),
			  KEY idx_products_date_available_zen (products_date_available),
			  KEY idx_products_ordered_zen (products_ordered),
			  KEY idx_products_model_zen (products_model),
			  KEY idx_products_price_sorter_zen (products_price_sorter),
			  KEY idx_master_categories_id_zen (master_categories_id),
			  KEY idx_products_sort_order_zen (products_sort_order),
			  KEY idx_manufacturers_id_zen (manufacturers_id)
	 
	 CREATE TABLE products (
			  products_id int(11) NOT NULL auto_increment,
			  products_type int(11) NOT NULL default '1',
			  products_quantity float NOT NULL default '0',
			  products_model varchar(32) default NULL,
			  products_image varchar(64) default NULL,
			  products_price decimal(15,4) NOT NULL default '0.0000',
			  products_virtual tinyint(1) NOT NULL default '0',
			  products_date_added datetime NOT NULL default '0001-01-01 00:00:00',
			  products_last_modified datetime default NULL,
			  products_date_available datetime default NULL,
			  products_weight float NOT NULL default '0',
			  products_status tinyint(1) NOT NULL default '0',
			  products_tax_class_id int(11) NOT NULL default '0',
			  manufacturers_id int(11) default NULL,
			  products_ordered float NOT NULL default '0',
			  products_quantity_order_min float NOT NULL default '1',
			  products_quantity_order_units float NOT NULL default '1',
			  products_priced_by_attribute tinyint(1) NOT NULL default '0',
			  product_is_free tinyint(1) NOT NULL default '0',
			  product_is_call tinyint(1) NOT NULL default '0',
			  products_quantity_mixed tinyint(1) NOT NULL default '0',
			  product_is_always_free_shipping tinyint(1) NOT NULL default '0',
			  products_qty_box_status tinyint(1) NOT NULL default '1',
			  products_quantity_order_max float NOT NULL default '0',
			  products_sort_order int(11) NOT NULL default '0',
			  products_discount_type tinyint(1) NOT NULL default '0',
			  products_discount_type_from tinyint(1) NOT NULL default '0',
			  products_price_sorter decimal(15,4) NOT NULL default '0.0000',
			  master_categories_id int(11) NOT NULL default '0',
			  products_mixed_discount_quantity tinyint(1) NOT NULL default '1',
			  metatags_title_status tinyint(1) NOT NULL default '0',
			  metatags_products_name_status tinyint(1) NOT NULL default '0',
			  metatags_model_status tinyint(1) NOT NULL default '0',
			  metatags_price_status tinyint(1) NOT NULL default '0',
			  metatags_title_tagline_status tinyint(1) NOT NULL default '0',
			  PRIMARY KEY  (products_id),
			  KEY idx_products_date_added_zen (products_date_added),
			  KEY idx_products_status_zen (products_status),
			  KEY idx_products_date_available_zen (products_date_available),
			  KEY idx_products_ordered_zen (products_ordered),
			  KEY idx_products_model_zen (products_model),
			  KEY idx_products_price_sorter_zen (products_price_sorter),
			  KEY idx_master_categories_id_zen (master_categories_id),
			  KEY idx_products_sort_order_zen (products_sort_order),
			  KEY idx_manufacturers_id_zen (manufacturers_id)
			) ENGINE=MyISAM;*/

}
