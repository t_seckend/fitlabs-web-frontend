package com.timbre.sec.model.products;

import com.timbre.common.AuditableObject;
import com.timbre.sec.model.web.content.GalleryJPA;

public interface IGallery extends AuditableObject{

	public void setId(Long id);
	
	public Long getId();
	public String getProductId();
	
	public void setProductId(String productId);
	
	public void setProductJPA(ProductJPA prod);
	
	public ProductJPA getProductJPA();
	
	public void setGalleryId(String galleryid);
	
	public String getGalleryId();

	public String getProductCode();
	
	public void setProductCode(String code);
	
	//public void setGallery(GalleryJPA gallery);
	
	//public GalleryJPA getGallery();

}
