package com.timbre.sec.model.products;

import com.timbre.common.AuditableObject;
import com.timbre.sec.model.web.content.DescriptionJPA;

public interface IDescription extends AuditableObject{

	public void setId(Long id);
	
	public Long getId();
	public String getProductId();
	
	public void setProductId();
	
	public void setProductJPA(ProductJPA prod);
	
	public ProductJPA getProductJPA();

	public String getProductCode();
	
	public void setProductCode(String code);
	
	public void setProdDesc(String descript);
	
	public String getProdDesc();
	
	public void setStyleNumber(String stylenumber);
	
	public String getStyleNumber();

}