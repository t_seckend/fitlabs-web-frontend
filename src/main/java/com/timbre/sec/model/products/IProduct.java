package com.timbre.sec.model.products;

import java.util.Collection;
import java.util.Set;

import com.timbre.common.AuditableObject;
import com.timbre.sec.model.web.content.DescriptionJPA;
import com.timbre.sec.model.web.content.GalleryJPA;

public interface IProduct extends AuditableObject{

	public String getProductId();

	public String getProductCode();
	
	public String getPrice();
	
	public String getName();
	
	public void setProductCode(String code);
	
	public Collection<String> getColors();
	
	public Collection<String> getSizes();
	
	public GalleryJPA getGallery();
	
	public DescriptionJPA getDescription();
	
	public String getStyleNumber();
	
	

}
