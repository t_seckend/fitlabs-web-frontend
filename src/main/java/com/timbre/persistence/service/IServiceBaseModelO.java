package com.timbre.persistence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.timbre.common.BaseModelObject;;

public interface IServiceBaseModelO < T extends BaseModelObject >{
	
	// find/get
	
	Optional<T> findOne( final long id );
	
	List< T > findAll();
	
	Page< T > findPaginated( final int page, final int size, final String sortBy );
	
	// save/create/persist
	
	T create( final T entity );
	
	// update/merge
	
	void update( final T entity );
	
	// delete
	
	void delete( final long id );
	
	void deleteAll();
	
}
