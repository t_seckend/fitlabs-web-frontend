package com.timbre.persistence.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.timbre.common.BaseModelObject;;

@Transactional
public abstract class AbstractBaseModelService< T extends BaseModelObject > implements IServiceBaseModelO< T >{
	protected final Logger logger = LoggerFactory.getLogger( this.getClass() );
	
	public AbstractBaseModelService(){
		super();
	}
	
	// API
	
	// find/get
	
	@Override
	@Transactional( readOnly = true )
	public Optional<T> findOne( final long id ){
		return this.getDao().findById( id );
	}
	
	@Override
	@Transactional( readOnly = true )
	public List< T > findAll(){
		return Lists.newArrayList( this.getDao().findAll() );
	}
	
	
	
	// save/create/persist
	
	@Override
	public T create( final T entity ){
		Preconditions.checkNotNull( entity );
		
		final T persistedEntity = this.getDao().save( entity );
		
		return persistedEntity;
	}
	
	// update/merge
	
	@Override
	public void update( final T entity ){
		Preconditions.checkNotNull( entity );
		
		this.getDao().save( entity );
	}
	
	// delete
	
	@Override
	public void deleteAll(){
		this.getDao().deleteAll();
	}
	@Override
	public void delete( final long id ){
		this.getDao().deleteById( id );
	}
	
	//
	protected abstract CrudRepository< T, Long > getDao();
	
}
