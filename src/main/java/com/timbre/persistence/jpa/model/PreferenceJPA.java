package com.timbre.persistence.jpa.model;


import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
//import org.hibernate.validator.NotNull;
import com.timbre.common.IEntity;
/*import com.timbre.persistence.jpa.api.model.Preference;
import com.timbre.persistence.jpa.api.model.User;
import com.timbre.sec.model.Principal;*/
import com.timbre.sec.model.products.ProductJPA;

@Entity
public class PreferenceJPA implements IEntity,  Serializable{
	
	protected UUID idOne;
	
	
	@Id
	private String principalId;
	
    

	private static final long serialVersionUID = 1376628118792909420L;
    
    @OneToOne
	private UserJPA user;
    
    private String email;
    
    private String name;
    
    // we no longer use principal
   // private Principal principal;
    
    private String firstName;
    
    private String lastName;
    
    private String streetAddress;
    
    private String streetAddress2;
    
    private String city;
    
    private String state;
    
    private String zip;
    
    private String country;
    
    
    private String billingStreetAddress;
    
    private String billingStreetAddress2;
    
    private String billingCity;
    
    private String billingState;
    
    private String billingZip;
    
    private String billingCountry;
    
        
    private String county;
    
    private String phone;
    
    private String moblePhone;
    
    private String size;
    
    private String age;
    
    private boolean emailList;
    
    private boolean shippingSameAsBilling;
    
    private String favColor;
    
    private String favActivity;
    
    private String selfDescrPersona; //Are you mostly Tomboy or Girly Girl
    
    
    
    
    public PreferenceJPA(){
    	this.setPreferenceId(generateUid());	
    }
    
    public PreferenceJPA(UserJPA user){
	this.user = user;
    	
    }
    

	public PreferenceJPA(String key, String value) {
		// TODO Auto-generated constructor stub
	}

	public UserJPA getUser() {
		return user;
	}

	public void setUser(UserJPA user) {
		this.user = user;
	}
	
	public String getPreferenceId() {
		return principalId;
	}

	public void setPreferenceId(String principalId) {
		this.principalId = principalId;
	}
    
    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBillingStreetAddress() {
		return billingStreetAddress;
	}

	public void setBillingStreetAddress(String billingStreetAddress) {
		this.billingStreetAddress = billingStreetAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMoblePhone() {
		return moblePhone;
	}

	public void setMoblePhone(String moblePhone) {
		this.moblePhone = moblePhone;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public boolean isEmailList() {
		return emailList;
	}

	public void setEmailList(boolean emailList) {
		this.emailList = emailList;
	}

	public boolean getShippingSameAsBilling() {
		return shippingSameAsBilling;
	}

	public void setShippingSameAsBilling(boolean shippingSameAsBilling) {
		this.shippingSameAsBilling = shippingSameAsBilling;
	}

	public String getFavColor() {
		return favColor;
	}

	public void setFavColor(String favColor) {
		this.favColor = favColor;
	}

	public String getFavActivity() {
		return favActivity;
	}

	public void setFavActivity(String favActivity) {
		this.favActivity = favActivity;
	}

	public String getSelfDescrPersona() {
		return selfDescrPersona;
	}

	public void setSelfDescrPersona(String selfDescrPersona) {
		this.selfDescrPersona = selfDescrPersona;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	public String generateUid() {
		idOne = UUID.randomUUID();
		return String.valueOf(idOne);
	}

	public String getPrincipalId() {
		return principalId;
	}

	/*public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public void setPrincipal(Principal entity) {
		this.principal = entity;
		
	}*/

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setBillingStreetAddress2(String billingStreetAddress2) {
		this.billingStreetAddress2 = billingStreetAddress2;
	}

	public String getBillingStreetAddress2() {
		return billingStreetAddress2;
	}
    
    
    
}
