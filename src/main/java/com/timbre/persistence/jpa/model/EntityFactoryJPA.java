/*package com.timbre.persistence.jpa.model;
import com.timbre.persistence.jpa.api.model.BinaryAttribute;
import com.timbre.persistence.jpa.api.model.Preference;
import com.timbre.persistence.jpa.api.model.StringAttribute;
import com.timbre.persistence.jpa.api.model.TextAttribute;
import com.timbre.persistence.jpa.api.model.Ticket;
import com.timbre.persistence.jpa.api.model.QName;
import com.timbre.persistence.jpa.api.model.Attribute;
import com.timbre.persistence.jpa.api.model.AvailabilityItem;
import com.timbre.persistence.jpa.api.model.AttributeTombstone;
import com.timbre.persistence.jpa.api.model.BooleanAttribute;
import com.timbre.persistence.jpa.api.model.CalendarAttribute;
import com.timbre.persistence.jpa.api.model.CalendarCollectionStamp;
import com.timbre.persistence.jpa.api.model.CollectionItem;
import com.timbre.persistence.jpa.api.model.CollectionItemDetails;
import com.timbre.persistence.jpa.api.model.CollectionSubscription;
import com.timbre.persistence.jpa.api.model.AvailabilityItem;
import com.timbre.persistence.jpa.api.model.ContentItem;
import com.timbre.persistence.jpa.api.model.EventExceptionStamp;
import com.timbre.persistence.jpa.api.model.EventStamp;
import com.timbre.persistence.jpa.api.model.FreeBusyItem;
import com.timbre.persistence.jpa.api.model.IntegerAttribute;
import com.timbre.persistence.jpa.api.model.ICalendarAttribute;
import com.timbre.persistence.jpa.api.model.ICalendarItem;
import com.timbre.persistence.jpa.api.model.Item;
import com.timbre.persistence.jpa.api.model.NoteItem;
import com.timbre.persistence.jpa.api.model.ItemTombstone;
import com.timbre.persistence.jpa.api.model.NoteOccurrence;
import com.timbre.persistence.jpa.api.model.TicketType;

import com.timbre.persistence.jpa.api.model.PasswordRecovery;
import com.timbre.persistence.jpa.api.model.Stamp;
import com.timbre.persistence.jpa.api.model.TaskStamp;
import com.timbre.persistence.jpa.api.model.TriageStatus;
import com.timbre.persistence.jpa.api.model.User;

import com.timbre.persistence.jpa.model.*;


import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.UUID;

import com.timbre.persistence.jpa.api.model.EntityFactory;

//import org.apache.commons.id.IdentifierGenerator;
//import org.apache.commons.id.uuid.VersionFourGenerator;



//import org.osaf.cosmo.model.XmlAttribute;
import org.w3c.dom.Element;

*//**
 * EntityFactory implementation that uses Hibernate 
 * persistent objects.
 *//*
public class EntityFactoryJPA implements EntityFactory {

    //private IdentifierGeneratorFactory idGenerator = new VersionFourGenerator();
	UUID idOne; 
	
	
	
		
    public CollectionItem createCollection() {
        return new CollectionItemJPA();
    }

    public NoteItem createNote() {
        return new NoteItemJPA();
    }

    public AvailabilityItem createAvailability() {
        return new AvailabilityItemJPA();
    }

    public BinaryAttribute createBinaryAttribute(QName qname, byte[] bytes) {
        return new BinaryAttributeJPA(qname, bytes);
    }

    public BinaryAttribute createBinaryAttribute(QName qname, InputStream is) {
        return new BinaryAttributeJPA(qname, is);
    }

    public CalendarAttribute createCalendarAttribute(QName qname, Calendar cal) {
        return new CalendarAttributeJPA(qname, cal);
    }

    public CalendarCollectionStamp createCalendarCollectionStamp(CollectionItem col) {
        return new CalendarCollectionStampJPA(col);
    }

    public CollectionSubscription createCollectionSubscription() {
        return new CollectionSubscriptionJPA();
    }

    public DecimalAttribute createDecimalAttribute(QName qname, BigDecimal bd) {
        return new DecimalAttributeJPA(qname, bd);
    }

    public EventExceptionStamp createEventExceptionStamp(NoteItem note) {
        return new EventExceptionStampJPA(note);
    }

    
    public FileItem createFileItem() {
        return new FileItemJPA();
    }

    public FreeBusyItem createFreeBusy() {
        return new FreeBusyItemJPA();
    }
    
    public EventStamp createEventStamp(NoteItem note) {
        return new EventStampJPA(note);
    }

    public IntegerAttribute createIntegerAttribute(QName qname, Long longVal) {
        return new IntegerAttributeJPA(qname, longVal);
    }

    public XmlAttribute createXMLAttribute(QName qname, Element e) {
        return null;//new HibXmlAttribute(qname, e);
    }

    public MessageStamp createMessageStamp() {
        return new MessageStampJPA();
    }

    public PasswordRecovery createPasswordRecovery(User user, String key) {
        return new PasswordRecoveryJPA(user, key);
    }

    public Preference createPreference() {
        return (Preference) new PreferenceJPA();
    }

    public Preference createPreference(String key, String value) {
        return (Preference) new PreferenceJPA(key, value);
    }

    public QName createQName(String namespace, String localname) {
        return new QNameJPA(namespace, localname);
    }
//
    public StringAttribute createStringAttribute(QName qname, String str) {
        return new StringAttributeJPA(qname, str);
    }
//
    public TaskStamp createTaskStamp() {
        return new TaskStampJPA();
    }
//
    public TextAttribute createTextAttribute(QName qname, Reader reader) {
        return new TextAttributeJPA(qname, reader);
    }
    
    public TaskStamp createTaskStamp() {
        return new TaskStampJPA();
    }

    public Ticket createTicket(TicketType type) {
        return new TicketJPA(type);
    }

    public TriageStatus createTriageStatus() {
        return new TriageStatusJPA();
    }

    public User createUser() {
        return new UserJPA();
    }

    public Ticket creatTicket() {
        return new TicketJPA();
    }
    
    public String generateUid() {
    	idOne = UUID.randomUUID();
        return String.valueOf(idOne);
    }

    public String generateUid() {
        return idGenerator.nextIdentifier().toString();
    }

}
*/