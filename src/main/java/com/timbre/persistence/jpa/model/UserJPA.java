package com.timbre.persistence.jpa.model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.persistence.CascadeType;






import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;




//import org.hibernate.validator.Email;
//import org.hibernate.validator.Length;
//import org.hibernate.validator.NotNull;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
/*import com.timbre.persistence.jpa.api.model.CollectionItem;
import com.timbre.persistence.jpa.api.model.CollectionSubscription;
import com.timbre.persistence.jpa.api.model.ModelValidationException;
import com.timbre.persistence.jpa.api.model.Preference;
import com.timbre.persistence.jpa.api.model.User;
import com.timbre.sec.model.Principal;
import com.timbre.sec.model.Role;*/
import com.timbre.sec.model.products.IProduct;
import com.timbre.sec.model.products.ProductJPA;
import com.timbre.sec.model.products.ProductReviewJPA;

import com.timbre.sec.model.web.content.GalleryJPA;


@Entity
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserJPA extends AuditableObjectJPA { //implements User {

   
	public boolean enableAcctSettingAjx = true;
	
    private static final long serialVersionUID = -5401963358519490736L;
   
    /**
     */
    public static final int USERNAME_LEN_MIN = 3;
    /**
     */
    public static final int USERNAME_LEN_MAX = 32;
   
    /**
     */
    public static final int FIRSTNAME_LEN_MIN = 1;
    /**
     */
    public static final int FIRSTNAME_LEN_MAX = 128;
    /**
     */
    public static final int LASTNAME_LEN_MIN = 1;
    /**
     */
    public static final int LASTNAME_LEN_MAX = 128;
    /**
     */
    public static final int EMAIL_LEN_MIN = 8;
    /**
     */
    public static final int EMAIL_LEN_MAX = 128;
    
    @Transient
    String sessionId = null;
    
    


    //@GeneratedValue( strategy = GenerationType.IDENTITY )
    @Index(name="idx_useruid")
    //private String uid = FACTORY.generateUid();
    
    // This did not work@AttributeOverride(name="username", column=@Column(name="name"))
    // originally coded with @Column(name = "username", nullable=false)
    //@Column(name = "name", nullable=false)
    //@Index(name="idx_username")
    //@NotNull
    //@NaturalId
    //@org.hibernate.validator.constraints.Length(min=USERNAME_LEN_MIN, max=USERNAME_LEN_MAX)
    //per bug 11599:
    // Usernames must be between 3 and 32 characters; may contain any Unicode
    //character in the following range of unicode code points: [#x20-#xD7FF] |
    //[#xE000-#xFFFD] EXCEPT #x7F or #x3A
    // Oh and don't allow ';' or '/' because there are problems with encoding
    // them in urls (tomcat doesn't support it)
    //@org.hibernate.validator.Pattern(regex="^[\\u0020-\\ud7ff\\ue000-\\ufffd&&[^\\u007f\\u003a;/\\\\]]+$")
    //@javax.validation.constraints.Pattern(regexp="^[\\u0020-\\ud7ff\\ue000-\\ufffd&&[^\\u007f\\u003a;/\\\\]]+$")
    private String name;
    
    private  String oldUsername;
    
    @Column(name = "password")
    @NotNull
    private String password;
    
    @Column(name = "firstname")
    @Length(min=FIRSTNAME_LEN_MIN, max=FIRSTNAME_LEN_MAX)
    private String firstName;
    
    @Column(name = "lastname")
    @Length(min=LASTNAME_LEN_MIN, max=LASTNAME_LEN_MAX)
    private String lastName;
    
    @Column(name = "email", nullable=false, unique=true)
    @Index(name="idx_useremail")
    @NotNull
    @Length(min=EMAIL_LEN_MIN, max=EMAIL_LEN_MAX)
    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
        +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
        +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9]*[a-z0-9])?",
             message="{invalid.email}")
    private String email;
    
    private  String oldEmail;
    
    @Column(name = "activationid", nullable=true, length=255)
    @Length(min=1, max=255)
    @Index(name="idx_activationid")
    private String activationId;
    
    private boolean activated;
    
    @Column(name = "admin")
    private Boolean admin;
    
    private  Boolean oldAdmin;
    
    @Column(name = "locked")
    private Boolean locked;
    
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
            message="{invalid.phonenumber}")
   protected String mobilePhone;
   @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
            message="{invalid.phonenumber}")
   protected String homePhone;
   @Temporal(javax.persistence.TemporalType.DATE)
   @Past
   protected Date birthday;
    
    
   @OneToOne(targetEntity=PreferenceJPA.class, mappedBy="user", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)  
	private PreferenceJPA preferenceJPA;
   
   /*@OneToOne(targetEntity=ItemJPA.class, mappedBy="owner", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)  
	private  ItemJPA homeCollectionJPA;
   //HomeCollectionItemJPA
*/ 
   
   /*@OneToMany(targetEntity=ItemJPA.class, cascade = { CascadeType.ALL }, mappedBy = "owner", fetch=FetchType.EAGER)
   @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
   private CollectionItem homeCollectionJPA = 
       (CollectionItem) new HashSet<CollectionItem>(0);
    
    public CollectionItem getHomeCollectionJPA() {
	return (CollectionItem) homeCollectionJPA;
}


public void setHomeCollectionJPA(CollectionItem homeCollectionJPA) {
	this.homeCollectionJPA =  homeCollectionJPA;
}*/


	// In this relationship the owner is an instance of and User interface or a UserJPA
    /*@OneToMany(targetEntity=CollectionSubscriptionJPA.class, cascade = { CascadeType.REMOVE }, mappedBy = "owner", fetch=FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<CollectionSubscription> subscriptions = 
        new HashSet<CollectionSubscription>(0);*/
    
    @OneToMany(targetEntity=ProductReviewJPA.class, cascade = { CascadeType.REMOVE }, mappedBy = "userJPA", fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ProductReviewJPA> reviewHistory = 
        new HashSet<ProductReviewJPA>(0);
    
    /*@OneToMany(targetEntity=OrderJPA.class, cascade = { CascadeType.REMOVE }, mappedBy = "userJPA", fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<OrderJPA> puchaseHistory = 
        new HashSet<OrderJPA>(0);*/
    
    /*@Transient
    ArrayList<OrderJPA> orders = new ArrayList<OrderJPA>();*/
    @Transient
    ArrayList<ProductReviewJPA> revs = new ArrayList<ProductReviewJPA>();
    
    //private Collection<GrantedAuthority> authorities;

    /******* Principal integration **************/
    
       
    /*@ManyToMany(  cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinTable(joinColumns = { @JoinColumn(name = "uid", referencedColumnName = "uid") }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID") })
    @XStreamImplicit
    private Set<Role> roles;*/
    
       
    
    /**
     */
    public UserJPA() {
    	super();
        admin = Boolean.FALSE;
        locked = Boolean.FALSE;
    }
    
    
	public UserJPA( final String nameToSet, final String passwordToSet)//,  final Set< Role > rolesToSet )
			{
		super();
		// We set email as the name to set for login
		email = nameToSet;
		password = passwordToSet;
		//roles =  rolesToSet;
	}
	
	

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getUid()
     */
    /*public String getUid() {
        return uid;
    }

     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setUid(java.lang.String)
     
    public void setUid(String uid) {
        this.uid = uid;
    }*/

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getUsername()
     */
    public String getUsername() {
        return name;//username;
    }

    public void setUsername(String name) {
        oldUsername = this.name;
        this.name = name;
    }

    
    public String getOldUsername() {
        return oldUsername != null ? oldUsername : name;
    }

    
    public boolean isUsernameChanged() {
        return oldUsername != null && ! oldUsername.equals(name);
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getPassword()
     */
    public String getPassword() {
        return password;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setPassword(java.lang.String)
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getFirstName()
     */
    public String getFirstName() {
        return firstName;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setFirstName(java.lang.String)
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getLastName()
     */
    public String getLastName() {
        return lastName;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setLastName(java.lang.String)
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getEmail()
     */
    public String getEmail() {
        return email;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setEmail(java.lang.String)
     */
    public void setEmail(String email) {
        oldEmail = this.email;
        this.email = email;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getOldEmail()
     */
    public String getOldEmail() {
        return oldEmail;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#isEmailChanged()
     */
    public boolean isEmailChanged() {
        return oldEmail != null && ! oldEmail.equals(email);
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getAdmin()
     */
    public Boolean getAdmin() {
        return admin;
    }
    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getOldAdmin()
     */
    public Boolean getOldAdmin() {
        return oldAdmin;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#isAdminChanged()
     */
    public boolean isAdminChanged() {
        return oldAdmin != null && ! oldAdmin.equals(admin);
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setAdmin(java.lang.Boolean)
     */
    public void setAdmin(Boolean admin) {
        oldAdmin = this.admin;
        this.admin = admin;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getActivationId()
     */
    public String getActivationId() {
        return activationId;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setActivationId(java.lang.String)
     */
    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#isOverlord()
     */
    /*public boolean isOverlord() {
        return email != null && email.equals(USERNAME_OVERLORD);
    }*/

    
    public boolean isActivated() {
        return activated;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#activate()
     */
    public void activate(){
       this.activated = true;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#isLocked()
     */
    public Boolean isLocked() {
        return locked;
    }
    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#setLocked(java.lang.Boolean)
     */
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    /**
     * Username determines equality 
     */
    /*@Override
    public boolean equals(Object obj) {
        if (obj == null || email == null)
            return false;
        if (! (obj instanceof User))
            return false;
        
        return email.equals(((User) obj).getUsername());
    }

    @Override
        public int hashCode() {
        if (email == null)
            return super.hashCode();
        else
            return email.hashCode();
    }*/

    /**
     */
    public String toString() {
        return new ToStringBuilder(this).
            append("username", name).
            append("password", "xxxxxx").
            append("firstName", firstName).
            append("lastName", lastName).
            append("email", email).
            append("admin", admin).
            append("activationId", activationId).
            append("locked", locked).
            toString();
    }

   
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#validateRawPassword()
     */
    /*public void validateRawPassword() {
        if (password == null) {
            throw new ModelValidationException(this, "Password not specified");
        }
        if (password.length() < PASSWORD_LEN_MIN ||
            password.length() > PASSWORD_LEN_MAX) {
            throw new ModelValidationException(this, "Password must be " +
                                               PASSWORD_LEN_MIN + " to " +
                                               PASSWORD_LEN_MAX +
                                               " characters in length");
        }
    }*/
    
    
    /*public Set<Preference> getPreferences() {
        return preference;
    }

    
    public void addPreference(Preference preference) {
        preference.setUser(this);
        preference.add(preference);
    }

    
    public Preference getPreference(String key) {
        for (Preference pref : preference) {
            if (pref.getKey().equals(key))
                return pref;
        }
        return null;
    }

    
    public void removePreference(String key) {
        removePreference(getPreference(key));
    }

    
    public void removePreference(Preference preference) {
        if (preference != null)
            preference.remove(preference);
    }*/
    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getCollectionSubscriptions()
     */
    /*public Set<CollectionSubscription> getCollectionSubscriptions() {
        return subscriptions;
    }

     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#addSubscription(org.osaf.cosmo.model.CollectionSubscription)
     
    public void addSubscription(CollectionSubscription subscription) {
        subscription.setOwner(this);
        subscriptions.add(subscription);
    }

     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getSubscription(java.lang.String)
     
    public CollectionSubscription getSubscription(String displayname) {

        for (CollectionSubscription sub : subscriptions) {
            if (sub.getDisplayName().equals(displayname))
                return sub;
        }

        return null;
    }
   
     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#getSubscription(java.lang.String, java.lang.String)
     
    public CollectionSubscription getSubscription(String collectionUid, String ticketKey){
        for (CollectionSubscription sub : subscriptions) {
            if (sub.getCollectionUid().equals(collectionUid)
                    && sub.getTicketKey().equals(ticketKey)) {
                return sub;
            }
        }

        return null;
    }

   
     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#removeSubscription(java.lang.String, java.lang.String)
     
    public void removeSubscription(String collectionUid, String ticketKey){
        removeSubscription(getSubscription(collectionUid, ticketKey));
    }
    
     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#removeSubscription(java.lang.String)
     
    public void removeSubscription(String displayName) {
        removeSubscription(getSubscription(displayName));
    }

     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#removeSubscription(org.osaf.cosmo.model.CollectionSubscription)
     
    public void removeSubscription(CollectionSubscription sub) {
        if (sub != null)
            subscriptions.remove(sub);
    }

     (non-Javadoc)
     * @see org.osaf.cosmo.model.User#isSubscribedTo(org.osaf.cosmo.model.CollectionItem)
     
    public boolean isSubscribedTo(CollectionItem collection){
        for (CollectionSubscription sub : subscriptions){
            if (collection.getUid().equals(sub.getCollectionUid())) return true;
        }
        return false;
    }

    public String calculateEntityTag() {
        String username = getUsername() != null ? getUsername() : "-";
        String modTime = getModifiedDate() != null ?
            new Long(getModifiedDate().getTime()).toString() : "-";
        String etag = username + ":" + modTime;
        return encodeEntityTag(etag.getBytes());
    }

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	public boolean getActivated() {
		return activated;
	}


	public void setPreferenceJPA(PreferenceJPA preference) {
		this.preferenceJPA = preference;
	}


	public PreferenceJPA getPreferenceJPA() {
		return preferenceJPA;
	}


	@Override
	public Set<Preference> getPreferences() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addPreference(Preference preference) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Preference getPreference(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void removePreference(String key) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removePreference(Preference preference) {
		// TODO Auto-generated method stub
		
	}


	public Set<Role> getRoles() {
		return roles;
	}


	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public boolean isEnableAcctSettingAjx() {
		return enableAcctSettingAjx;
	}
	
	public boolean getEnableAcctSettingAjx() {
		return enableAcctSettingAjx;
	}

	public void setEnableAcctSettingAjx(boolean enableAcctSettingAjx) {
		this.enableAcctSettingAjx = enableAcctSettingAjx;
	}


	public String getSessionId() {
		return sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	
	public void setPuchaseHistory(Set<OrderJPA> puchaseHistory) {
		this.puchaseHistory = puchaseHistory;
	}
	
	public Set<OrderJPA> getPurchaseHistory(){
		return puchaseHistory;
	}


	public ArrayList<OrderJPA> getOrders() {
		
		//Arrays.asList(puchaseHistory.toArray());
		
		//ArrayList<OrderJPA> orders = new ArrayList<OrderJPA>();
		//List<OrderJPA> listObject = Arrays.asList(puchaseHistory.toArray());
		for (Iterator<OrderJPA> itOverPurch = puchaseHistory.iterator(); itOverPurch.hasNext();) {
			Object obj = itOverPurch.next();
			OrderJPA e = (OrderJPA) obj;
			orders.add(e);
		}
			
		
		
		return orders;
	}*/


	public void setReviewHistory(Set<ProductReviewJPA> reviewHistory) {
		this.reviewHistory = reviewHistory;
	}
	
	public Set<ProductReviewJPA> getReviewHistory(){
		return reviewHistory;
	}


	public ArrayList<ProductReviewJPA> getRevs() {
		//ArrayList<ProductReviewJPA> revs = new ArrayList<ProductReviewJPA>();
		//List<OrderJPA> listObject = Arrays.asList(puchaseHistory.toArray());
		for (Iterator<ProductReviewJPA> itOverRev = reviewHistory.iterator(); itOverRev.hasNext();) {
			Object obj = itOverRev.next();
			ProductReviewJPA e = (ProductReviewJPA) obj;
			revs.add(e);
		}
		
		return revs;
	}


	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}


	/*public HomeCollectionItemJPA getHomeCollectionJPA() {
		return homeCollectionJPA;
	}


	public void setHomeCollectionJPA(HomeCollectionItemJPA homeCollectionJPA) {
		this.homeCollectionJPA = homeCollectionJPA;
	}*/


	
}

