package com.timbre.persistence.jpa.model;


import java.security.MessageDigest;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
//import com.timbre.persistence.jpa.api.model.AuditableObject;
//import com.timbre.persistence.jpa.model.EntityFactoryJPA;



@MappedSuperclass
public abstract class AuditableObjectJPA extends BaseModelObjectJPA implements com.timbre.common.AuditableObject {

    private static final ThreadLocal<MessageDigest> etagDigestLocal = new ThreadLocal<MessageDigest>();
    private static final Base64 etagEncoder = new Base64();
   // protected static final EntityFactoryJPA FACTORY = new EntityFactoryJPA();
    
    @Column(name = "createdate")
    //@Type(type="long_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Column(name = "modifydate")
    //@Type(type="long_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    
    @Column(name="etag")
    private String etag = "";
    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.AuditableObject#getCreationDate()
     */
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.AuditableObject#getModifiedDate()
     */
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.AuditableObject#updateTimestamp()
     */
    public void updateTimestamp() {
        modifiedDate = new Date();
    }
    
    /* (non-Javadoc)
     * @see org.osaf.cosmo.model.AuditableObject#getEntityTag()
     */
    public String getEntityTag() {
        return etag;
    }
    
    public void setEntityTag(String etag) {
        this.etag = etag;
    }
    
    /**
     * Calculates object's entity tag. Returns the empty string. Subclasses should override this.
     */
    public String calculateEntityTag() {
        return "";
    }

    /**
     * <p>
     * Returns a Base64-encoded SHA-1 digest of the provided bytes.
     * </p>
     */
    protected static String encodeEntityTag(byte[] bytes) {
        
        // Use MessageDigest stored in threadlocal so that each
        // thread has its own instance.
        MessageDigest md = etagDigestLocal.get();
        
        if(md==null) {
            try {
                // initialize threadlocal
                md = MessageDigest.getInstance("sha1");
                etagDigestLocal.set(md);
            } catch (Exception e) {
                throw new RuntimeException("Platform does not support sha1?", e);
            }
        }
        
        return new String(etagEncoder.encode(md.digest(bytes)));
    }
    
   /* public EntityFactoryJPA getFactory() {
        return FACTORY;
    }*/
}
