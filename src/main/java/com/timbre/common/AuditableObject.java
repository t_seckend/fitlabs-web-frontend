package com.timbre.common;

import java.util.Date;

//import org.osaf.cosmo.model.EntityFactory;

/**
 * Represents a model object.
 */
public interface AuditableObject extends IEntity{

    /**
     * @return date object was created
     */
    public Date getCreationDate();

    /**
     * @return date object was last updated
     */
    public Date getModifiedDate();

    /**
     * Update modifiedDate with current system time.
     */
    public void updateTimestamp();

    /**
     * <p>
     * Returns a string representing the state of the object. Entity tags can
     * be used to compare two objects in remote environments where the
     * equals method is not available.
     * </p>
     */
    public String getEntityTag();

    /**
     * @return EntityFactory that was used to create object.
     */
    //public EntityFactory getFactory();

}
