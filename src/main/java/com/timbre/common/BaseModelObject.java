package com.timbre.common;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//import org.apache.commons.lang3.builder.ToStringBuilder;
//import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Base class for model objects.
 */
//@MappedSuperclass
public abstract class BaseModelObject implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
   // private Long id = new Long(-1);
    private Long id; 
    
    /**
     */
   /* public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }*/

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }
}
