package com.timbre.spring.persistence.jpa;

import java.util.Properties;

import javax.sql.DataSource;

//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
//@ImportResource( "classpath*:*springDataConfig.xml" )

//@EnableJpaRepositories(basePackages="com.timbre, com.fitlabs", entityManagerFactoryRef="entityManagerFactory")
public class PersistenceJPAConfig{

	@Value( "${jdbc.driverClassName}" )
	private String driverClassName;

	@Value( "${jdbc.url}" )
	private String url;

	@Value( "${hibernate.dialect}" )
	String hibernateDialect;

	//@Value( "${hibernate.show_sql}" )
	boolean hibernateShowSql = false;

	@Value( "${hibernate.hbm2ddl.auto}" )
	String hibernateHbm2ddlAuto;

	@Value( "${jpa.generateDdl}" )
	boolean jpaGenerateDdl;
	
	/*@Value("${hibernate.cache.use_second_level_cache}")
	boolean second_level_cache = true;*/
	
	@Value("${hibernate.cache.use_query_cache}")
	boolean use_query_cache = true;
	
	//@Value("${foreign_key_checks}")
	//int foreign_key_checks = 0;
	//foreign_key_checks
	
//hibernate.cache.use_query_cache=true")

	public PersistenceJPAConfig(){
		super();
	}

	// beans
	
	@Bean 
    public HibernateExceptionTranslator hibernateExceptionTranslator(){ 
      return new HibernateExceptionTranslator(); 
    }

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
		final LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource( dataSource() );
		entityManagerFactory.setPackagesToScan(new String[] {
				"com.timbre",
				"com.timbre.persistence",
				"com.fitlabs.login"
				});

		final JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter(){
			{
				//setDatabase( Database.H2 ); // TODO: comment this out, see if anything fails
				setDatabase( Database.MYSQL );
				setDatabasePlatform( hibernateDialect );
				setShowSql( hibernateShowSql );
				setGenerateDdl( jpaGenerateDdl );
			}
		};
		entityManagerFactory.setJpaVendorAdapter( vendorAdapter );

		entityManagerFactory.setJpaProperties( additionlProperties() );

		return entityManagerFactory;
	}

	@Bean
	public DataSource dataSource(){
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName( driverClassName );
		dataSource.setUrl( url );
	    dataSource.setUsername( "root" );
		dataSource.setPassword( "@M0bl3y!" );
		return dataSource;
	}

	@Bean
	public JpaTransactionManager transactionManager(){
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory( entityManagerFactory().getObject() );

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
		return new PersistenceExceptionTranslationPostProcessor();
	}

	//
	final Properties additionlProperties(){
		return new Properties(){
			{
				// use this to inject additional properties in the EntityManager
				setProperty("hibernate.cache.use_second_level_cache", "org.hibernate.cache.RegionFactory");
				
				setProperty("hibernate.cache.use_query_cache", "org.hibernate.cache.RegionFactory");
				
			}
		};
	}

}
